import * as dateFormat from 'dateformat';

export function generateUsername(): string {
  return `test_${generateSnapshotTime()}`;
}

export function generateEmail(): string {
  return `test_${generateSnapshotTime()}@mail.ru`;
}

function generateSnapshotTime(): string {
  return dateFormat(new Date(), 'yyyy-MM-dd\'_\'HH-MM-ss');
}
