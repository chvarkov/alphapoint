import { Type } from '@nestjs/common';

/**
 * Temp solution.
 * @link https://github.com/facebook/jest/issues/1700
 */
export async function asyncExpectThrow<T>(callback: () => Promise<any>, exceptionType: Type<T>): Promise<void> {
    try {
        await callback();
    } catch (e) {
        expect(e).toBeInstanceOf(exceptionType);
        return;
    }
    fail('But it didn\'t throw anything.');
}
