export interface ITestUser {
  userId: number;
  accountId: number;
  username: string;
  email: string;
}

export interface ITestAccountContaining {
  accountIdContainingTrades: number;
  accountIdContainingDepositTickets: number;
  accountIdContainingWithdrawTickets: number;
}

export class TestUserProvider {

  static user: ITestUser;

  static get accounts(): ITestAccountContaining {
    return {
      accountIdContainingTrades: 388,
      accountIdContainingDepositTickets: 498,
      accountIdContainingWithdrawTickets: 152,
    };
  }

  static getUser(): ITestUser {
    if (this.user) {
      return this.user;
    }

    return {
      userId: 502,
      accountId: 	516,
      username: 'test_2019-08-02_13-42-35',
      email: 'test_2019-08-02_13-42-35@mail.ru',
    };
  }

  static setUser(user: ITestUser): TestUserProvider {
    this.user = user;

    return this;
  }
}
