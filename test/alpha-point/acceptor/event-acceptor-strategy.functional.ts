import { ReplyAcceptorStrategy } from '@alpha-point/services/acceptor/strategies/reply.acceptor-strategy';
import { ErrorCodesEnum } from '@alpha-point/enums/error-codes.enum';
import { Message, Result } from '@alpha-point/models/message';
import { MessageTypeEnum } from '@alpha-point/enums/message-type.enum';
import { IAcceptionContext } from '@alpha-point/interfaces/acceptor-strategy.interface';
import { PublicMethodsEnum } from '@alpha-point/enums/common/public-methods.enum';
import { EventAcceptorStrategy } from '@alpha-point/services/acceptor/strategies/event.acceptor-strategy';
import { asyncExpectThrow } from '../../helpers/async-expect-throw.helper';
import { UnexpectedResponseException } from '@alpha-point/exceptions/unexpected-response.exception';
import { EventsEnum } from '@alpha-point/enums/events.enum';
import { UnexpectedEventSnapshotException } from '@alpha-point/exceptions/unexpected-event-snapshot.exception';

describe('ReplyAcceptorStrategy', () => {
  it('Positive', async () => {
    await testSubscription('testEvent', 'testEvent', 'testSnapshot', 'testSnapshot');
  });

  it('Negative: Unexpected response', async () => {
    await asyncExpectThrow( async () => testSubscription('testEvent', 'OTHER', 'testSnapshot', 'OTHER'), UnexpectedResponseException);
  });

  it('Negative: Unexpected event snapshot', async () => {
    await asyncExpectThrow( async () => testSubscription('testEvent', 'testEvent', 'testSnapshot', 'OTHER'), UnexpectedEventSnapshotException);
  });
});

function testSubscription(expectedEvent: string, actualEvent: string, expectedSnapshot: string, actualSnapshot: string): Promise<void> {
  return new Promise<any>((resolve, reject) => {
    const context: IAcceptionContext = {
      requests: [
        {
          types: {
            request: null,
            response: null,
          },
          resolve,
          reject,
        },
      ],
      subscriptions: {},
    };

    context.subscriptions[expectedEvent] = {
      event: EventsEnum.Level1UpdateEvent,
      response: null,
      request: null,
      callbacks: {},
      snapshot: expectedSnapshot,
      snapshotCallback: m => actualSnapshot,
    };

    context.subscriptions[expectedEvent].callbacks[expectedSnapshot] = (d) => resolve(d);

    const message = Object.assign(new Message(), {
      index: 0,
      method: actualEvent,
      type: MessageTypeEnum.Event,
      payloadJson: '[1, 2, 3, 4, 5]',
    });

    (new EventAcceptorStrategy(context)).accept(message);
  });

}
