import { InternalServerErrorException } from '@nestjs/common';
import { AcceptorFactory } from '@alpha-point/services/acceptor/acceptor.factory';
import { MessageTypeEnum } from '@alpha-point/enums/message-type.enum';
import { ReplyAcceptorStrategy } from '@alpha-point/services/acceptor/strategies/reply.acceptor-strategy';
import { EventAcceptorStrategy } from '@alpha-point/services/acceptor/strategies/event.acceptor-strategy';
import { ErrorAcceptorStrategy } from '@alpha-point/services/acceptor/strategies/error.acceptor-strategy';
import { IAcceptionContext } from '@alpha-point/interfaces/acceptor-strategy.interface';

describe('AcceptorFactory', () => {
  const emptyContext: IAcceptionContext = {requests: [], subscriptions: {}};

  it('Positive', () => {
    expect(AcceptorFactory.create(MessageTypeEnum.Reply, emptyContext)).toBeInstanceOf(ReplyAcceptorStrategy);
    expect(AcceptorFactory.create(MessageTypeEnum.Event, emptyContext)).toBeInstanceOf(EventAcceptorStrategy);
    expect(AcceptorFactory.create(MessageTypeEnum.Error, emptyContext)).toBeInstanceOf(ErrorAcceptorStrategy);
  });

  it('Negative: Unexpected type', () => {
    expect(() => AcceptorFactory.create(-1, emptyContext)).toThrow(InternalServerErrorException);
  });
});
