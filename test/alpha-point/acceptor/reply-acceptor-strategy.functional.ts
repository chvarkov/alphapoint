import { ReplyAcceptorStrategy } from '@alpha-point/services/acceptor/strategies/reply.acceptor-strategy';
import { ErrorCodesEnum } from '@alpha-point/enums/error-codes.enum';
import { Message, Result } from '@alpha-point/models/message';
import { MessageTypeEnum } from '@alpha-point/enums/message-type.enum';
import { serialize } from 'class-transformer';
import { asyncExpectThrow } from '../../helpers/async-expect-throw.helper';
import { AlphaPointNotAuthorizedException } from '@alpha-point/exceptions/alpha-point/alpha-point-not-authorized.exception';
import { AlphaPointInvalidResponseException } from '@alpha-point/exceptions/alpha-point/alpha-point-invalid-response.exception';
import { AlphaPointServerErrorException } from '@alpha-point/exceptions/alpha-point/alpha-point-server-error.exception';
import { AlphaPointNotFoundException } from '@alpha-point/exceptions/alpha-point/alpha-point-not-found.exception';
import { AlphaPointOperationFailedException } from '@alpha-point/exceptions/alpha-point/alpha-point-operation-failed.exception';
import { BadGatewayException } from '@nestjs/common';
import { UnexpectedResponseException } from '@alpha-point/exceptions/unexpected-response.exception';

describe('ReplyAcceptorStrategy', () => {
  it('Negative: Exception types', async () => {
    await asyncExpectThrow(async () => accept(createFailedMessage(ErrorCodesEnum.NotAuthorized)), AlphaPointNotAuthorizedException);
    await asyncExpectThrow(async () => accept(createFailedMessage(ErrorCodesEnum.InvalidResponse)), AlphaPointInvalidResponseException);
    await asyncExpectThrow(async () => accept(createFailedMessage(ErrorCodesEnum.ServerError)), AlphaPointServerErrorException);
    await asyncExpectThrow(async () => accept(createFailedMessage(ErrorCodesEnum.NotFound)), AlphaPointNotFoundException);
    await asyncExpectThrow(async () => accept(createFailedMessage(ErrorCodesEnum.OperationFailed)), AlphaPointOperationFailedException);
    await asyncExpectThrow(async () => accept(createFailedMessage(0)), BadGatewayException);
  });

  it('Negative: Invalid payload json', async () => {
    const message = Object.assign(new Message<Result>(), {
      index: 0,
      type: MessageTypeEnum.Reply,
      payloadJson: 'Invalid JSON',
    });
    await asyncExpectThrow(async () => accept(message), UnexpectedResponseException);
  });

  it('Negative: Unexpected request index', async () => {
    const message = Object.assign(new Message<Result>(), {
      index: -1,
      type: MessageTypeEnum.Reply,
      payloadJson: '{}',
    });

    (new ReplyAcceptorStrategy({requests: [], subscriptions: {}})).accept(message);
  });
});

function accept(message: Message<any>): Promise<any> {
  return new Promise<any>((resolve, reject) => {
    const context = {
      requests: [{
        types: {
          request: null,
          response: Result,
        },
        resolve,
        reject,
      }],
      subscriptions: {},
    };

    (new ReplyAcceptorStrategy(context)).accept(message);
  });
}

function createFailedMessage(errorCode: ErrorCodesEnum): Message<Result> {
  const result = Object.assign(new Result(), {
    result: false,
    errorCode,
  });
  return Object.assign(new Message<Result>(), {
    index: 0,
    type: MessageTypeEnum.Reply,
    payloadJson: serialize(result),
  });
}
