import { InternalServerErrorException } from '@nestjs/common';
import { PublicMethodsEnum } from '@alpha-point/enums/common/public-methods.enum';
import { InstrumentAwareRequest } from '@alpha-point/models/message';
import { SubscriptionRequestBuilder } from '@alpha-point/services/builders/subscription-request.builder';
import { Level1EventData } from '@alpha-point/models/users';
import { EventsEnum } from '@alpha-point/enums/events.enum';
import { IMessage } from '@alpha-point/interfaces/message.interface';

describe('SubscriptionRequestBuilder Functional', () => {
  it('Positive', () => {
    const request = new SubscriptionRequestBuilder(InstrumentAwareRequest, Level1EventData)
      .setData({ instrumentId: 1 })
      .setMethod(PublicMethodsEnum.SubscribeLevel1)
      .setEvent(EventsEnum.Level1UpdateEvent)
      .setCallback(m => m)
      .setSnapshot('snapshot')
      .setSnapshotCallback((m: IMessage<Level1EventData>) => m.payloadJson)
      .build();

    expect(request.data).toBeInstanceOf(InstrumentAwareRequest);
    expect(request.method).toEqual(PublicMethodsEnum.SubscribeLevel1);
    expect(request.event).toEqual(EventsEnum.Level1UpdateEvent);
    expect(request.event).toEqual(EventsEnum.Level1UpdateEvent);
    expect(request.snapshot).toEqual('snapshot');
    expect(request.snapshotCallback).not.toEqual(null);
    expect(request.request).toEqual(InstrumentAwareRequest);
    expect(request.response).toEqual(Level1EventData);
  });

  it('Positive: Build without request, response types', () => {
    const request = new SubscriptionRequestBuilder()
        .setData({ instrumentId: 1 })
        .setMethod(PublicMethodsEnum.SubscribeLevel1)
        .setEvent(EventsEnum.Level1UpdateEvent)
        .setCallback(m => m)
        .setSnapshot('snapshot')
        .setSnapshotCallback((m: IMessage<Level1EventData>) => m.payloadJson)
        .build();

    expect(request.request).toEqual(null);
    expect(request.response).toEqual(null);

    expect(request.data).not.toBeInstanceOf(InstrumentAwareRequest);
    expect(request.data).toEqual({ instrumentId: 1 });

    expect(request.method).toEqual(PublicMethodsEnum.SubscribeLevel1);
    expect(request.event).toEqual(EventsEnum.Level1UpdateEvent);
    expect(request.event).toEqual(EventsEnum.Level1UpdateEvent);
    expect(request.snapshot).toEqual('snapshot');
    expect(request.snapshotCallback).not.toEqual(null);

  });

  it('Negative: Build without event', () => {
    expect(() => {
      new SubscriptionRequestBuilder(InstrumentAwareRequest, Level1EventData)
        .setData({ instrumentId: 1 })
        .setMethod(PublicMethodsEnum.SubscribeLevel1)
        .setCallback(m => m)
        .setSnapshot('snapshot')
        .setSnapshotCallback((m: IMessage<Level1EventData>) => m.payloadJson)
        .build();
    }).toThrow(InternalServerErrorException);
  });

  it('Negative: Build without callback', () => {
    expect(() => {
      new SubscriptionRequestBuilder(InstrumentAwareRequest, Level1EventData)
        .setData({ instrumentId: 1 })
        .setMethod(PublicMethodsEnum.SubscribeLevel1)
        .setEvent(EventsEnum.Level1UpdateEvent)
        .setSnapshot('snapshot')
        .setSnapshotCallback((m: IMessage<Level1EventData>) => m.payloadJson)
        .build();
    }).toThrow(InternalServerErrorException);
  });

  it('Negative: Build without snapshot', () => {
    expect(() => {
      new SubscriptionRequestBuilder(InstrumentAwareRequest, Level1EventData)
        .setData({ instrumentId: 1 })
        .setMethod(PublicMethodsEnum.SubscribeLevel1)
        .setEvent(EventsEnum.Level1UpdateEvent)
        .setCallback(m => m)
        .setSnapshotCallback((m: IMessage<Level1EventData>) => m.payloadJson)
        .build();
    }).toThrow(InternalServerErrorException);
  });

  it('Negative: Build without snapshot callback', () => {
    expect(() => {
      new SubscriptionRequestBuilder(InstrumentAwareRequest, Level1EventData)
        .setData({ instrumentId: 1 })
        .setMethod(PublicMethodsEnum.SubscribeLevel1)
        .setEvent(EventsEnum.Level1UpdateEvent)
        .setCallback(m => m)
        .setSnapshot('snapshot')
        .build();
    }).toThrow(InternalServerErrorException);
  });
});
