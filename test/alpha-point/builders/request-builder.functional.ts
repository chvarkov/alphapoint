import { InternalServerErrorException } from '@nestjs/common';
import { RequestBuilder } from '@alpha-point/services/builders/request.builder';
import { PublicMethodsEnum } from '@alpha-point/enums/common/public-methods.enum';
import { InstrumentAwareRequest } from '@alpha-point/models/message';
import { Instrument } from '@alpha-point/models/instruments';

describe('RequestBuilder Functional', () => {
  it('Positive', () => {
    const request = new RequestBuilder(InstrumentAwareRequest, Instrument)
      .setMethod(PublicMethodsEnum.GetInstrument)
      .setData({instrumentId: 1})
      .build();

    expect(request.data).toBeInstanceOf(InstrumentAwareRequest);
    expect(request.method).toEqual(PublicMethodsEnum.GetInstrument);
  });

  it('Positive: Build without data', () => {
    const request = new RequestBuilder(InstrumentAwareRequest, Instrument)
      .setMethod(PublicMethodsEnum.GetInstrument)
      .build();

    expect(request.method).toEqual(PublicMethodsEnum.GetInstrument);
  });

  it('Negative: Build without method', () => {
    expect(() => {
      new RequestBuilder(InstrumentAwareRequest, Instrument)
        .setData({instrumentId: 1})
        .build();
    }).toThrow(InternalServerErrorException);
  });
});
