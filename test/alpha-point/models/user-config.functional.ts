import { UserConfig } from '@alpha-point/models/users/user-config';
import { InternalServerErrorException } from '@nestjs/common';

describe('UserConfig model', () => {

    const items = [
        {key: 'Key1', value: 'Value1'},
        {key: 'Key2', value: 'Value2'},
    ];
    const userConfig = new UserConfig(items);

    it('Positive: get', () => {
        expect(userConfig.get('Key1')).toEqual('Value1');
        expect(userConfig.get('Null')).toEqual(null);
    });

    it('Positive: getAll', () => {
        expect(userConfig.getAll()).toEqual(items);
    });

    it('Positive: set, has, remove', () => {
        expect(userConfig.set('Key3', 'Value3')).toBeInstanceOf(UserConfig);
        expect(userConfig.has('Key3')).toEqual(true);

        expect(userConfig.remove('Key3'));
        expect(userConfig.has('Key3')).toEqual(false);

        const removedItem = userConfig.getAll().find(item => item.key === 'Key3');
        expect(!!removedItem).toEqual(true);
        expect(removedItem.value).toEqual('');
    });

    it('Negative: remove', () => {
        expect(() => userConfig.remove('Key4')).toThrow(InternalServerErrorException);
    });
});
