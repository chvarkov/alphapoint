import { serialize } from 'class-transformer';
import { Message, Result } from '@alpha-point/models/message';
import { ErrorCodesEnum } from '@alpha-point/enums/error-codes.enum';

describe('Message model', () => {

    it('Positive: getPayload', () => {
        const result = Object.assign(new Result(), {
            result: false,
            errorMessage: ErrorCodesEnum.NotFound,
            errorCode: 'Resource not found',
            detail: null,
        });

        const message = new Message<Result>();
        message.payloadJson = serialize(result);

        const deserializedResult = message.getPayload(Result);
        expect(deserializedResult).toEqual(result);
    });

    it('Positive: getPayload without parameter', () => {
        const expectedArray = [1, 2, 3, 4, 5];

        const message = new Message();
        message.payloadJson = JSON.stringify(expectedArray);

        const actualArray = message.getPayload();
        expect(actualArray).toBeInstanceOf(Array);
        expect(actualArray).toEqual(expectedArray);
    });
});
