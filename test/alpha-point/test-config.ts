import { IModuleOptions } from '@alpha-point/interfaces/options.interfaces';

// TODO: Remove hard code
export const  MODULE_OPTIONS: IModuleOptions = {
  private: {
    wss: 'wss://apindaxstage.cdnhop.net:10456/WSAdminGateway',
    omsId: 1,
    credentials: {
      username: 'ndaxstagingoperator',
      password: 'Sw1zzleNd@x',
    },
    attemptCount: 3,
    reconnectTimeout: 2000,
  },
  public: {
    wss: 'wss://apindaxstage.cdnhop.net/WSGateway',
    omsId: 1,
    attemptCount: 3,
    reconnectTimeout: 2000,
  },
};
