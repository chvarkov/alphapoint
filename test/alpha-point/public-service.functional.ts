import { PublicService } from '@alpha-point/services/public.service';
import { Connection } from '@alpha-point/services/connection';
import { MODULE_OPTIONS } from './test-config';
import { asyncExpectThrow } from '../helpers/async-expect-throw.helper';
import { UnexpectedResponseException } from '@alpha-point/exceptions/unexpected-response.exception';
import { ISubscriptionRequest } from '@alpha-point/interfaces/request.interface';
import { IMessage } from '@alpha-point/interfaces/message.interface';
import { Level1EventData } from '@alpha-point/models/users';
import { serialize } from 'class-transformer';
import { Message } from '@alpha-point/models/message';
import { MessageTypeEnum } from '@alpha-point/enums/message-type.enum';
import { EventsEnum } from '@alpha-point/enums/events.enum';
import { L2EventDataActionTypeEnum, L2SnapshotSideEnum } from '@alpha-point/enums/l2-snapshot-side.enum';
import { Level2EventData } from '@alpha-point/models/normalized';
import { WebSocketFactory } from '@alpha-point/services/web-socket.factory';

describe('AlphaPoint PublicService Functional', () => {
  it('Negative: Unexpected response types',  async () => {
    const connectionMock: Connection = Object.assign(new  Connection(MODULE_OPTIONS.public, new WebSocketFactory()), {
      send: jest.fn().mockReturnValue(Promise.resolve(null)),
    });

    const publicService = new PublicService(connectionMock);

    await asyncExpectThrow(async () => publicService.getInstruments(), UnexpectedResponseException);

    connectionMock.send = jest.fn().mockReturnValue(Promise.resolve([]));

    await asyncExpectThrow(async () => publicService.getProduct({productId: 1}), UnexpectedResponseException);
  });

  it('Positive: Snapshot in Level1 subscription', async () => {
    const data = Object.assign(new Level1EventData(), {
      exchangeId: 1,
      instrumentId: 2,
      bestBid: 3,
      bestOffer: 4,
      lastTradedPx: 5,
      lastTradedQty: 6,
      lastTradeTime: 7,
      sessionOpen: 8,
      sessionHigh: 9,
      sessionLow: 10,
      volume: 11,
      currentDayVolume: 12,
      currentDayNumTrades: 13,
      currentDayPxChange: 14,
      rolling24HrVolume: 15,
      rolling24NumTrades: 16,
      rolling24HrPxChange: 17,
      timeStamp: 153563566566,
    });

    const message = Object.assign(new Message<Level1EventData>(), {
      type: MessageTypeEnum.Event,
      index: 2,
      method: EventsEnum.Level1UpdateEvent,
      payloadJson: serialize(data),
    });

    await testEventSnapshot(message, 'InstrumentId:2').subscribeLevel1({instrumentId: 2}, e => e);
  });

  it('Positive: Snapshot in Level2 subscription', async () => {
    const productPairCode = 2;
    const data = [
      [1, 123, 1501604532000, L2EventDataActionTypeEnum.New, 2667.95, 27, 2700.81, productPairCode, 2871, L2SnapshotSideEnum.Buy],
      [1, 123, 1501604532000, L2EventDataActionTypeEnum.Deletion, 2667.95, 27, 2700.81, productPairCode, 2871, L2SnapshotSideEnum.Sell],
      [1, 123, 1501604532000, L2EventDataActionTypeEnum.Update, 2667.95, 27, 2700.81, productPairCode, 2871, L2SnapshotSideEnum.Short],
    ];

    const message = Object.assign(new Message<Level2EventData>(), {
      type: MessageTypeEnum.Event,
      index: 4,
      method: EventsEnum.Level2UpdateEvent,
      payloadJson: serialize(data),
    });

    await testEventSnapshot(message, 'InstrumentId:2').subscribeLevel2({instrumentId: 2}, e => e);
  });

  it('Positive: Snapshot in Ticker subscription', async () => {
    const instrumentId = 2;
    const data = [
      [1501604532000, 2792.73, 2667.95, 2687.01, 2700.81, 242.61340767, 0, 2871, instrumentId],
      [1501604532000, 2792.73, 2667.95, 2687.01, 2700.81, 242.61340767, 0, 2871, instrumentId],
      [1501604532000, 2792.73, 2667.95, 2687.01, 2700.81, 242.61340767, 0, 2871, instrumentId],
   ];

    const message = Object.assign(new Message<number[]>(), {
      type: MessageTypeEnum.Reply,
      index: 4,
      method: EventsEnum.TickerDataUpdateEvent,
      payloadJson: serialize(data),
    });

    await testEventSnapshot(message, 'InstrumentId:2').subscribeTicker({instrumentId: 2}, e => e);
  });

  it('Positive: Snapshot in Trades subscription', async () => {
    const productPairCode = 2;
    const data = [
      [1713390, productPairCode, 0.25643269, 6419.77, 203100209, 203101083, 1534863265752, 2, 1, 0, 0],
      [1713390, productPairCode, 0.25643269, 6419.77, 203100209, 203101083, 1534863265752, 2, 1, 0, 0],
      [1713390, productPairCode, 0.25643269, 6419.77, 203100209, 203101083, 1534863265752, 2, 1, 0, 0],
    ];

    const message = Object.assign(new Message<number[]>(), {
      type: MessageTypeEnum.Reply,
      index: 4,
      method: EventsEnum.OrderTradeEvent,
      payloadJson: serialize(data),
    });

    await testEventSnapshot(message, 'InstrumentId:2').subscribeTrades({instrumentId: 2}, e => e);
  });
});

function testEventSnapshot(message: IMessage<any>, expectedSnapshot: string): PublicService {
  const connectionMock: Connection = Object.assign(new  Connection(MODULE_OPTIONS.public, new WebSocketFactory()), {
    send: (request: ISubscriptionRequest<any, any>) => {
      const snapshot = request.snapshotCallback(message);

      expect(snapshot).toEqual(expectedSnapshot);
      return message.getPayload(request.response);
    },
  });

  return new PublicService(connectionMock);
}
