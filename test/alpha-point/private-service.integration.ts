import { AlphaPointModule } from '@alpha-point/alpha-point.module';
import { Test } from '@nestjs/testing';
import { MODULE_OPTIONS } from './test-config';
import { PrivateService } from '@alpha-point/services/private.service';
import { Logger } from '@nestjs/common';
import { UserAffiliateCount, UserInfo, UserConfig, UserRegisterRequest, Authenticate2FA } from '@alpha-point/models/users';
import { AlphaPointNotFoundException } from '@alpha-point/exceptions/alpha-point/alpha-point-not-found.exception';
import { asyncExpectThrow } from '../helpers/async-expect-throw.helper';
import { generateEmail, generateUsername } from '../helpers/generator.helper';
import { TestUserProvider } from './test-user-provider';
import { AlphaPointOperationFailedException } from '@alpha-point/exceptions/alpha-point/alpha-point-operation-failed.exception';
import { AccountInfo, AccountPositionItem } from '@alpha-point/models/accounts';
import { AccountTrade } from '@alpha-point/models/trades';
import { DepositTicket, Transaction, WithdrawTicket } from '@alpha-point/models/tickets';

describe('AlphaPoint PrivateService Integration', () => {

  const NONEXISTENT_USER_ID = 4173851;
  const NONEXISTENT_USERNAME = '14c4b06b824ec593239362517f538b29';

  let app;
  let privateService: PrivateService;
  let testUser = TestUserProvider.getUser();
  const testAccounts = TestUserProvider.accounts;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [
        AlphaPointModule.forRoot(MODULE_OPTIONS),
      ],
    }).compile();

    app = module.createNestApplication();
    await app.init();

    privateService = module.get<PrivateService>(PrivateService);
  });

  /**
   * @see PrivateService.registerUser
   */
  describe('RegisterUser', () => {
    it('Positive', async () => {
      const generatedUsername = generateUsername();
      const generatedEmail = generateEmail();

      const data: UserRegisterRequest = {
        operatorId: 1,
        reCaptcha: 'test recaptha',
        affiliateTag: 'Test',
        info: {
          username: generatedUsername,
          email: generatedEmail,
          passwordHash: 'Pa$$w0rd ha$h',
        },
      };

      await privateService.registerUser(data);

      const userInfo = await privateService.getUserInfo({username: generatedUsername});
      expect(userInfo.email).toEqual(generatedEmail);
      expect(userInfo.username).toEqual(generatedUsername);

      const {userId, accountId, email, username} = userInfo;

      TestUserProvider.setUser({ userId, accountId, email, username });
      testUser = TestUserProvider.getUser();
    });

    it('Positive: Empty data', async () => {
      const response = await privateService.getUserAccounts({ userId: NONEXISTENT_USER_ID, username: NONEXISTENT_USERNAME });
      expect(response).toBeInstanceOf(Array);
      expect(response).toEqual([]);
    });
  });

  /**
   * @see PrivateService.setVerificationLevel
   */
  describe('SetUserVerificationLevel', () => {
    it('Positive', async () => {
      await privateService.setVerificationLevel({ userId: testUser.userId, verificationLevel: 1 });
    });
  });

  /**
   * @see PrivateService.authenticate2fa
   */
  describe('Authenticate2FA', () => {
    it('Negative', async () => {
      const response = await privateService.authenticate2fa({ code: '123567' });
      expect(response).toBeInstanceOf(Authenticate2FA);
      expect(response.authenticated).toEqual(false);
    });
  });

  /**
   * @see PrivateService.forceUserLogoff
   */
  describe('ForceUserLogoff', () => {
    it('Positive', async () => {
      await privateService.forceUserLogoff({username: testUser.username});
    });

    it('Negative', async () => {
      await privateService.forceUserLogoff({username: NONEXISTENT_USERNAME});
    });
  });

  /**
   * @see PrivateService.resetPassword
   */
  describe('ResetPassword', () => {
    it('Positive', async () => {
      await privateService.resetPassword({username: testUser.username, reCaptcha: '11232434'});
    });

    it('Negative: Not found', async () => {
      await asyncExpectThrow(async () => {
        return  privateService.resetPassword({username: NONEXISTENT_USERNAME, reCaptcha: '11232434'});
      }, AlphaPointNotFoundException);
    });
  });

  /**
   * @see PrivateService.resendVerificationEmail
   */
  describe('ResendVerificationEmail', () => {
    it('Positive', async () => {
      await privateService.resendVerificationEmail({email: testUser.email});
    });

    it('Negative: Not found', async () => {
      await asyncExpectThrow(async () => {
        return  privateService.resendVerificationEmail({email: `${NONEXISTENT_USERNAME}@mail.ru`});
      }, AlphaPointOperationFailedException);
    });
  });

  /**
   * @see PrivateService.getUserAccounts
   */
  describe('GetUserAccounts', () => {
    it('Positive', async () => {
      const {userId, username} = testUser;
      const response = await privateService.getUserAccounts({ userId, username });
      expect(response).toBeInstanceOf(Array);
      expect(response.length).toBeGreaterThanOrEqual(1);
      expect(response.shift()).toBeGreaterThanOrEqual(1);
    });

    it('Positive: Empty data', async () => {
      const response = await privateService.getUserAccounts({ userId: NONEXISTENT_USER_ID, username: NONEXISTENT_USERNAME });
      expect(response).toBeInstanceOf(Array);
      expect(response).toEqual([]);
    });
  });

  /**
   * @see PrivateService.getAccountPositions
   */
  describe('GetAccountPositions', () => {
    it('Positive', async () => {
      const response = await privateService.getAccountPositions({accountId: testUser.accountId});
      expect(response).toBeInstanceOf(Array);
      expect(response.length).toBeGreaterThanOrEqual(1);
      expect(response.shift()).toBeInstanceOf(AccountPositionItem);
    });

    it('Negative: Empty data', async () => {
      const response = await privateService.getAccountPositions({accountId: NONEXISTENT_USER_ID});
      expect(response).toBeInstanceOf(Array);
      expect(response.length).toBeGreaterThanOrEqual(1);

      const first = response.shift();
      expect(first).toBeInstanceOf(AccountPositionItem);
      expect(first.accountName).toEqual(null);
    });
  });

  /**
   * @see PrivateService.getLoggedInUserBySessionToken
   */
  describe('GetLoggedInUserBySessionToken', () => {
    it('Negative: Not found user by session token', async () => {
      await asyncExpectThrow(async () => {
        await privateService.getLoggedInUserBySessionToken({ sessionToken: '00492a07-6791-4d1c-bfca-5cce3d0b9e92' });
      }, AlphaPointNotFoundException);
    });
  });

  /**
   * @see PrivateService.getUserAffiliateCount
   */
  describe('GetUserAffiliateCount', () => {
    it('Positive', async () => {
      const response = await privateService.getUserAffiliateCount({ userId: testUser.userId });
      expect(response).toBeInstanceOf(UserAffiliateCount);
    });

    it('Positive: Empty data', async () => {
      const response = await privateService.getUserAffiliateCount({ userId: NONEXISTENT_USER_ID });
      expect(response).toBeInstanceOf(UserAffiliateCount);
    });
  });

  /**
   * @see PrivateService.getAvailablePermissionList
   */
  describe('GetAvailablePermissionList', () => {
    it('Positive', async () => {
      const response = await privateService.getAvailablePermissionList();
      expect(response).toBeInstanceOf(Array);
      expect(response.length).toBeGreaterThanOrEqual(1);
      expect(typeof response.shift()).toEqual('string');
    });
  });

  /**
   * @see PrivateService.getUserInfo
   */
  describe('GetUserInfo', () => {
    it('Positive', async () => {
      const response = await privateService.getUserInfo({userId: testUser.userId});
      expect(response).toBeInstanceOf(UserInfo);
      expect(response.userId).toEqual(testUser.userId);
    });

    it('Negative: Not found', async () => {
      await asyncExpectThrow(async () => await privateService.getUserInfo({ userId: NONEXISTENT_USER_ID }), AlphaPointNotFoundException);
    });
  });

  /**
   * @see PrivateService.getAccountInfo
   */
  describe('GetAccountInfo', () => {
    it('Positive', async () => {
      const response = await privateService.getAccountInfo({accountId: testUser.accountId});
      expect(response).toBeInstanceOf(AccountInfo);
      expect(response.accountId).toEqual(testUser.accountId);
    });

    it('Negative: Not found', async () => {
      await asyncExpectThrow(async () => privateService.getAccountInfo({accountId: NONEXISTENT_USER_ID}), AlphaPointNotFoundException);
    });
  });

  /**
   * @see PrivateService.getUserPermissions
   */
  describe('GetUserPermissions', () => {
    it('Positive', async () => {
      const response = await privateService.getUserPermissions({userId: testUser.userId});
      expect(response).toBeInstanceOf(Array);
      expect(response.length).toBeGreaterThanOrEqual(1);
      expect(typeof response.shift()).toEqual('string');
    });

    it('Negative: Empty permissions', async () => {
      const permissions = await privateService.getUserPermissions({ userId: NONEXISTENT_USER_ID });
      expect(permissions).toBeInstanceOf(Array);
      expect(permissions.length).toEqual(0);
    });
  });

  /**
   * @see PrivateService.getAccountTrades
   */
  describe('GetAccountTrades', () => {
    it('Positive', async () => {
      const response = await privateService.getAccountTrades({accountId: testAccounts.accountIdContainingTrades, count: 5, startIndex: 0});
      expect(response).toBeInstanceOf(Array);
      expect(response.length).toEqual(5);

      const first = response.shift();
      expect(first).toBeInstanceOf(AccountTrade);
      expect(first.accountId).toEqual(388);
    });

    it('Negative: Empty', async () => {
      const response = await privateService.getAccountTrades({ accountId: NONEXISTENT_USER_ID, count: 5, startIndex: 0});
      expect(response).toBeInstanceOf(Array);
      expect(response.length).toEqual(0);
    });
  });

  /**
   * @see PrivateService.getAllWithdrawTickets
   */
  describe('GetAllWithdrawTickets', () => {
    it('Positive', async () => {
      const response = await privateService.getAllWithdrawTickets({operatorId: 1});
      expect(response).toBeInstanceOf(Array);
      expect(response.length).toBeGreaterThanOrEqual(1);

      const first = response.shift();
      expect(first).toBeInstanceOf(WithdrawTicket);
    });

    it('Negative: Empty', async () => {
      const response = await privateService.getAllWithdrawTickets({operatorId: NONEXISTENT_USER_ID});
      expect(response).toBeInstanceOf(Array);
      expect(response.length).toEqual(0);
    });
  });

  /**
   * @see PrivateService.getAccountWithdrawTransactions
   */
  describe('GetAccountWithdrawTransactions', () => {
    it('Positive', async () => {
      const response = await privateService.getAccountWithdrawTransactions({accountId: testAccounts.accountIdContainingWithdrawTickets, depth: 1});
      expect(response).toBeInstanceOf(Array);
      expect(response.length).toEqual(1);
      const first = response.shift();
      expect(first).toBeInstanceOf(Transaction);
    });

    it('Negative', async () => {
      const response = await privateService.getAccountWithdrawTransactions({accountId: testUser.accountId, depth: 1});
      expect(response).toBeInstanceOf(Array);
      expect(response.length).toEqual(0);
    });
  });

  /**
   * @see PrivateService.getAccountDepositTransactions
   */
  describe('GetAccountDepositTransactions', () => {
    it('Positive', async () => {
      const response = await privateService.getAccountDepositTransactions({accountId: testAccounts.accountIdContainingDepositTickets, depth: 1});
      expect(response).toBeInstanceOf(Array);
      expect(response.length).toEqual(1);
      const first = response.shift();
      expect(first).toBeInstanceOf(Transaction);
    });

    it('Negative', async () => {
      const response = await privateService.getAccountDepositTransactions({accountId: testUser.accountId, depth: 200});
      expect(response).toBeInstanceOf(Array);
      expect(response.length).toEqual(0);
    });
  });

  /**
   * @see PrivateService.getAllDepositTickets
   */
  describe('GetAllDepositTickets', () => {
    it('Positive', async () => {
      const response = await privateService.getAllDepositTickets({operatorId: 1});
      expect(response).toBeInstanceOf(Array);
      expect(response.length).toBeGreaterThanOrEqual(1);
      const first = response.shift();
      expect(first).toBeInstanceOf(DepositTicket);
    });

    it('Negative', async () => {
      const response = await privateService.getAllDepositTickets({operatorId: NONEXISTENT_USER_ID});
      expect(response).toBeInstanceOf(Array);
      expect(response.length).toEqual(0);
    });
  });

  /**
   * @see PrivateService.getUserConfig
   * @see PrivateService.setUserConfig
   */
  describe('GetUserConfig SetUserConfig', () => {
    const hiddenValue = '********';

    it('Positive get/set', async () => {
      const {userId, username} = testUser;
      const config = await privateService.getUserConfig({userId, username});

      expect(config).toBeInstanceOf(UserConfig);
      expect(config.getAll().length).toBeGreaterThanOrEqual(0);

      config
        .set('TestItem1', 'Test1')
        .set('TestItem2', 'Test2')
        .remove('TestItem2');

      await privateService.setUserConfig({userId, username, config: config.getChanges()});

      const result = await privateService.getUserConfig({userId, username});
      expect(result).toBeInstanceOf(UserConfig);
      expect(result.getAll().length).toBeGreaterThanOrEqual(1);

      const configItem = result.getAll().filter(item => item.key === 'TestItem1').shift();

      expect(!!configItem).toEqual(true);

      configItem.value === hiddenValue
        ? Logger.warn(`User config values is hidden (${hiddenValue}).`)
        : expect(configItem.value).toEqual('Test1');
    });

    it('Negative: GetUserConfig not found', async () => {
      await asyncExpectThrow(async () => {
        return await privateService.getUserConfig({ userId: NONEXISTENT_USER_ID, username: NONEXISTENT_USERNAME });
      }, AlphaPointNotFoundException);
    });

    it('Negative: SetUserConfig not found', async () => {
      await asyncExpectThrow(async () => {
        return await privateService.setUserConfig({ userId: NONEXISTENT_USER_ID, username: NONEXISTENT_USERNAME, config: [] });
      }, AlphaPointNotFoundException);
    });
  });

  /**
   * @see PrivateService.getAllUnredactedUserConfigsForUser
   */
  describe('GetAllUnredactedUserConfigsForUser', () => {
    it('Positive', async () => {
      const {userId, username} = testUser;
      const response = await privateService.getAllUnredactedUserConfigsForUser({userId, username});
      expect(response).toBeInstanceOf(UserConfig);
      expect(response.getAll().length).toBeGreaterThanOrEqual(0);
    });

    it('Negative: Not found', async () => {
      await asyncExpectThrow(async () => {
        return await privateService.getAllUnredactedUserConfigsForUser({ userId: NONEXISTENT_USER_ID, username: NONEXISTENT_USERNAME });
      }, AlphaPointNotFoundException);
    });
  });
});
