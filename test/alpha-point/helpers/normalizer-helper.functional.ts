import { RegisterUserInfo } from '@alpha-point/models/users';
import { arrayDeserialize } from '@alpha-point/helpers/deserializer.helper';
import { serialize } from 'class-transformer';

describe('Normalizer helper functions', () => {

  it('Positive: Deserialize array', () => {

    const expectModel = new RegisterUserInfo({username: 'User', email: 'mail@mail.ru', passwordHash: 'pa$$w0rd'});
    const json = serialize(expectModel);

    const array = arrayDeserialize(RegisterUserInfo, [json, json, json]);
    expect(array).toBeInstanceOf(Array);
    expect(array.length).toEqual(3);
    expect(array.shift()).toEqual(expectModel);
  });

  it('Positive: Deserialize array with empty array', () => {
    const array = arrayDeserialize(RegisterUserInfo, []);
    expect(array).toBeInstanceOf(Array);
    expect(array.length).toEqual(0);
  });

  it('Positive: Deserialize array null', () => {
    const array = arrayDeserialize(RegisterUserInfo, null);
    expect(array).toBeInstanceOf(Array);
    expect(array.length).toEqual(0);
  });
});
