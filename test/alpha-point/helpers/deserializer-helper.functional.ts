import { Ticker } from '@alpha-point/models/normalized';
import { denormalize, denormalizeArray, normalize, normalizeArray } from '@alpha-point/helpers/normalizer.helper';

describe('Deserializer helper functions', () => {

  const expectArray = [1501604532000, 2792.73, 2667.95, 2687.01, 2700.81, 242.61340767, 0, 2871, 0];
  const expectModel = new Ticker({
    time : expectArray[0],
    high : expectArray[1],
    low: expectArray[2],
    open : expectArray[3],
    close : expectArray[4],
    volume : expectArray[5],
    insideBidPrice : expectArray[6],
    insideAskPrice : expectArray[7],
    instrumentId : expectArray[8],
  });
  const expectDoubleArray = [expectArray, expectArray, expectArray];
  const expectModels = [expectModel, expectModel, expectModel];

  it('Positive: Normalize', () => {
    const model = normalize(Ticker, expectArray);
    expect(model).toBeInstanceOf(Ticker);
    expect(model).toEqual(expectModel);
  });

  it('Positive: Denormalize', () => {
    const array = denormalize(expectModel);
    expect(array).toEqual(expectArray);
  });

  it('Positive: Normalize array', () => {
    const models = normalizeArray(Ticker, expectDoubleArray);
    expect(models.length).toEqual(expectModels.length);
    expect(models[0]).toBeInstanceOf(Ticker);
    expect(models).toEqual(expectModels);
  });

  it('Positive: Denormalize array', () => {
    const array = denormalizeArray(expectModels);
    expect(array.length).toEqual(expectModels.length);
    expect(array).toEqual(expectDoubleArray);
  });

  it('Negative: Denormalize array', () => {
    const array = denormalizeArray([]);
    expect(array.length).toEqual(0);
  });
});
