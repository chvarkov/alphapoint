import { NormalizerFactory } from '@alpha-point/services/normalizer/normalizer.factory';
import { Level2EventData, Ticker, Trade } from '@alpha-point/models/normalized';
import { TickerNormalizerStrategy } from '@alpha-point/services/normalizer/strategies/ticker.normalizer-strategy';
import { InternalServerErrorException } from '@nestjs/common';
import { TradeNormalizerStrategy } from '@alpha-point/services/normalizer/strategies/trade.normalizer-strategy';
import { L2EventDataNormalizerStrategy } from '@alpha-point/services/normalizer/strategies/l2-event-data.normalizer-strategy';

describe('NormalizerFactory', () => {

  it('Positive', () => {
    expect(NormalizerFactory.create(Ticker)).toBeInstanceOf(TickerNormalizerStrategy);
    expect(NormalizerFactory.create(Trade)).toBeInstanceOf(TradeNormalizerStrategy);
    expect(NormalizerFactory.create(Level2EventData)).toBeInstanceOf(L2EventDataNormalizerStrategy);
  });

  it('Negative: Unexpected type', () => {
    expect(() => NormalizerFactory.create(Object)).toThrow(InternalServerErrorException);
  });

  it('Negative: null', () => {
    expect(() => NormalizerFactory.create(null)).toThrow(InternalServerErrorException);
  });
});
