import { Level2EventData } from '@alpha-point/models/normalized';
import { InternalServerErrorException } from '@nestjs/common';
import { L2EventDataActionTypeEnum, L2SnapshotSideEnum } from '@alpha-point/enums/l2-snapshot-side.enum';
import { L2EventDataNormalizerStrategy } from '@alpha-point/services/normalizer/strategies/l2-event-data.normalizer-strategy';

describe('L2EventDataNormalizeStrategy', () => {

  const strategy = new L2EventDataNormalizerStrategy(Level2EventData);

  const expectArray = [1, 123, 1501604532000, L2EventDataActionTypeEnum.New, 2667.95, 27, 2700.81, 1, 2871, L2SnapshotSideEnum.Buy];
  const expectModel = new Level2EventData({
    mdUpdateId: expectArray[0],
    accountId: expectArray[1],
    actionTimestamp: expectArray[2],
    actionType: expectArray[3],
    lastTradePrice: expectArray[4],
    orderId: expectArray[5],
    price: expectArray[6],
    productPairCode: expectArray[7],
    quantity: expectArray[8],
    side: expectArray[9],
  });

  it('Positive: Normalize / denormalize', () => {
    const normalizedModel = strategy.normalize(expectArray);
    expect(normalizedModel).toBeInstanceOf(Level2EventData);
    expect(normalizedModel).toEqual(expectModel);

    const array = strategy.denormalize(normalizedModel);
    expect(array).toEqual(expectArray);
  });

  it('Positive: Normalize array / denormalize array', () => {
    const expectDoubleArray = [expectArray, expectArray, expectArray];
    const expectModelArray = [expectModel, expectModel, expectModel];

    const normalizedModels = strategy.normalizeArray(expectDoubleArray);
    expect(normalizedModels.length).toEqual(expectDoubleArray.length);
    expect(normalizedModels[0]).toBeInstanceOf(Level2EventData);
    expect(normalizedModels).toEqual(expectModelArray);

    const doubleArray = strategy.denormalizeArray(normalizedModels);
    expect(doubleArray).toEqual(expectDoubleArray);
  });

  it('Negative: Unexpected type', () => {
    expect(() => strategy.denormalize({} as Level2EventData)).toThrow(InternalServerErrorException);
  });
});
