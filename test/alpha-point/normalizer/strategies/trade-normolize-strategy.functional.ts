import { TradeNormalizerStrategy } from '@alpha-point/services/normalizer/strategies/trade.normalizer-strategy';
import { Trade } from '@alpha-point/models/normalized';

describe('TradeNormalizeStrategy', () => {

  const strategy = new TradeNormalizerStrategy(Trade);

  const expectArray = [ 1713390, 1, 0.25643269, 6419.77, 203100209, 203101083, 1534863265752, 2, 1, 0, 0];
  const expectModel = new Trade({
    tradeId: expectArray[0],
    productPairCode: expectArray[1],
    quantity: expectArray[2],
    price: expectArray[3],
    order1: expectArray[4],
    order2: expectArray[5],
    tradeTime: expectArray[6],
    direction: expectArray[7],
    takerSide: expectArray[8],
    blockTrade: !!expectArray[9],
    orderClientId: expectArray[10],
  });

  it('Positive: Normalize / denormalize', () => {
    const normalizedModel = strategy.normalize(expectArray);
    expect(normalizedModel).toBeInstanceOf(Trade);
    expect(normalizedModel).toEqual(expectModel);

    const array = strategy.denormalize(normalizedModel);
    expect(array).toEqual(expectArray);
  });

  it('Positive: Normalize array / denormalize array', () => {
    const expectDoubleArray = [expectArray, expectArray, expectArray];
    const expectModelArray = [expectModel, expectModel, expectModel];

    const normalizedModels = strategy.normalizeArray(expectDoubleArray);
    expect(normalizedModels.length).toEqual(expectDoubleArray.length);
    expect(normalizedModels[0]).toBeInstanceOf(Trade);
    expect(normalizedModels).toEqual(expectModelArray);

    const doubleArray = strategy.denormalizeArray(normalizedModels);
    expect(doubleArray).toEqual(expectDoubleArray);
  });
});
