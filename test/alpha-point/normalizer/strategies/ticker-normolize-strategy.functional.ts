import { Ticker } from '@alpha-point/models/normalized';
import { TickerNormalizerStrategy } from '@alpha-point/services/normalizer/strategies/ticker.normalizer-strategy';
import { InternalServerErrorException } from '@nestjs/common';

describe('TickerNormalizeStrategy', () => {

  const strategy = new TickerNormalizerStrategy(Ticker);

  const expectArray = [1501604532000, 2792.73, 2667.95, 2687.01, 2700.81, 242.61340767, 0, 2871, 0];
  const expectModel = new Ticker({
    time : expectArray[0],
    high : expectArray[1],
    low: expectArray[2],
    open : expectArray[3],
    close : expectArray[4],
    volume : expectArray[5],
    insideBidPrice : expectArray[6],
    insideAskPrice : expectArray[7],
    instrumentId : expectArray[8],
  });

  it('Positive: Normalize / denormalize', () => {
    const normalizedModel = strategy.normalize(expectArray);
    expect(normalizedModel).toBeInstanceOf(Ticker);
    expect(normalizedModel).toEqual(expectModel);

    const array = strategy.denormalize(normalizedModel);
    expect(array).toEqual(expectArray);
  });

  it('Positive: Normalize array / denormalize array', () => {
    const expectDoubleArray = [expectArray, expectArray, expectArray];
    const expectModelArray = [expectModel, expectModel, expectModel];

    const normalizedModels = strategy.normalizeArray(expectDoubleArray);
    expect(normalizedModels.length).toEqual(expectDoubleArray.length);
    expect(normalizedModels[0]).toBeInstanceOf(Ticker);
    expect(normalizedModels).toEqual(expectModelArray);

    const doubleArray = strategy.denormalizeArray(normalizedModels);
    expect(doubleArray).toEqual(expectDoubleArray);
  });

  it('Negative: Unexpected type', () => {
    expect(() => strategy.denormalize({} as Ticker)).toThrow(InternalServerErrorException);
  });
});
