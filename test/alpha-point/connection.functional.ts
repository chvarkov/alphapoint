import { MODULE_OPTIONS } from './test-config';
import { Connection } from '@alpha-point/services/connection';
import { Message } from '@alpha-point/models/message';
import { RequestBuilder } from '@alpha-point/services/builders/request.builder';
import { WebSocketFactory } from '@alpha-point/services/web-socket.factory';
import { asyncExpectThrow } from '../helpers/async-expect-throw.helper';
import { WebAuthenticateUser } from '@alpha-point/models/users';
import { PrivateMethodsEnum } from '@alpha-point/enums/common/private-methods.enum';
import { MessageTypeEnum } from '@alpha-point/enums/message-type.enum';
import { IConnectionOptions } from '@alpha-point/interfaces/options.interfaces';
import { UnexpectedResponseException } from '@alpha-point/exceptions/unexpected-response.exception';
import { BadGatewayException } from '@nestjs/common';
import { EventsEnum } from '@alpha-point/enums/events.enum';

jest.setTimeout(20000);

describe('', () => {
  const wsFactory = new WebSocketFactory();

  it('Positive: Reconnection', async () => {
    const connection = await new Connection(MODULE_OPTIONS.public, wsFactory).connect();
    emitErrors(connection, MODULE_OPTIONS.public.attemptCount);
  });

  test('Negative: Unexpected response when authentication', async () => {
    const connectionForMock = await new Connection(MODULE_OPTIONS.private, wsFactory);
    const mockSendConnection: Connection = Object.assign(connectionForMock, {
      send: jest.fn().mockReturnValue(new Message<WebAuthenticateUser>({
        method: PrivateMethodsEnum.WebAuthenticateUser,
        index: 22,
        type: MessageTypeEnum.Reply,
        payloadJson: '[]',
      })),
    });

    await asyncExpectThrow(async () => mockSendConnection.connect(), UnexpectedResponseException);
  });

  test('Negative: Incorrect url to wss', async () => {
    const connectionOptions: IConnectionOptions = {
      ...MODULE_OPTIONS.public,
      wss: 'wss://failed-url',
      attemptCount: 1,
      reconnectTimeout: 0,
    };

    await asyncExpectThrow(async () => new Connection(connectionOptions, wsFactory).connect(), BadGatewayException);
  });

  test('Negative: Closed web socket client', async () => {
    const connectionOptions: IConnectionOptions = {
      ...MODULE_OPTIONS.public,
      attemptCount: 1,
      reconnectTimeout: 0,
    };
    const connection = await new Connection(connectionOptions, wsFactory).connect();
    emitCloseConnection(connection);

    const request = new RequestBuilder()
      .setMethod(EventsEnum.LogoutEvent)
      .build();

    await asyncExpectThrow(async () => connection.send(request), BadGatewayException);
  });
});

function emitErrors(connection, count: number): void {
  for (let i = 0; i < count; i++) {
    connection.wssClient.emit('error', new Error('Test'));
  }
}

function emitCloseConnection(connection) {
    connection.wssClient.close();
}
