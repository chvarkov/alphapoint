import { MODULE_OPTIONS } from './test-config';
import { BadGatewayException } from '@nestjs/common';
import { asyncExpectThrow } from '../helpers/async-expect-throw.helper';
import { Connection, EndpointType } from '@alpha-point/services/connection';
import { RequestBuilder } from '@alpha-point/services/builders/request.builder';
import { IConnectionOptions } from '@alpha-point/interfaces/options.interfaces';
import { FailedAuthenticationException } from '@alpha-point/exceptions/failed-authentication.exception';
import { Message } from '@alpha-point/models/message';
import { serialize } from 'class-transformer';
import { WebAuthenticateUser } from '@alpha-point/models/users';
import { EventsEnum } from '@alpha-point/enums/events.enum';
import { MessageTypeEnum } from '@alpha-point/enums/message-type.enum';
import { WebSocketFactory } from '@alpha-point/services/web-socket.factory';

describe('AlphaPoint Connection Integration', () => {

  const wsFactory = new WebSocketFactory();
  let connection: Connection;

  beforeAll(async () => {
    connection = await new Connection(MODULE_OPTIONS.private, wsFactory).connect();
  });

  test('Negative: Expect failed auth exception', async () => {
    const connectionOptions: IConnectionOptions = {
      ...MODULE_OPTIONS.private,
      credentials: {
        username: 'fsdfs',
        password: 'sdfs',
      },
    };

    await asyncExpectThrow(async () => new Connection(connectionOptions, wsFactory).connect(), FailedAuthenticationException);
  });

  test('Negative: Expect exception on request to nonexistent endpoint', async () => {
    const request = new RequestBuilder()
      .setMethod('NonExist' as EndpointType)
      .setData({})
      .build();

    await asyncExpectThrow(async () => connection.send(request), BadGatewayException);
  });

  test('Negative: On emit Logout event', async () => {
    const message = new Message<WebAuthenticateUser>({
      method: EventsEnum.LogoutEvent,
      index: 22,
      type: MessageTypeEnum.Event,
      payloadJson: JSON.stringify({ message: 'Logged out' }),
    });

    emitEvent(message, connection);
  });
});

function emitEvent<T>(message: Message<T>, connection) {
  connection.accept({data: serialize(message), type: 'message', target: connection.wssClient});
}
