import { AlphaPointModule } from '@alpha-point/alpha-point.module';
import { Test } from '@nestjs/testing';
import { PublicService } from '@alpha-point/services/public.service';
import { MODULE_OPTIONS } from './test-config';
import { asyncExpectThrow } from '../helpers/async-expect-throw.helper';
import { AlphaPointServerErrorException } from '@alpha-point/exceptions/alpha-point/alpha-point-server-error.exception';
import { AlphaPointNotFoundException } from '@alpha-point/exceptions/alpha-point/alpha-point-not-found.exception';
import { EmptyResponseException } from '@alpha-point/exceptions/empty-response.exception';
import { Product } from '@alpha-point/models/products';
import { Instrument } from '@alpha-point/models/instruments';
import { Level1, Level1EventData } from '@alpha-point/models/users';

describe('AlphaPoint PublicService Integration', () => {

  let app;
  let publicService: PublicService;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [
        AlphaPointModule.forRoot(MODULE_OPTIONS),
      ],
    }).compile();

    app = module.createNestApplication();
    await app.init();

    publicService = module.get<PublicService>(PublicService);
  });

  /**
   * @see PublicService.getProduct
   */
  describe('GetProduct', () => {
    it('Positive', async () => {
      const response = await publicService.getProduct({productId: 1});
      expect(response).toBeInstanceOf(Product);
    });

    it('Negative: Not found', async () => { // TODO: Not correct behavior of AlphaPoint
      await asyncExpectThrow(async () => publicService.getProduct({productId: 1545}), AlphaPointServerErrorException);
    });
  });

  /**
   * @see PublicService.getProducts
   */
  describe('GetProducts', () => {
    it('Positive', async () => {
      const response = await publicService.getProducts();
      expect(response.length).toBeGreaterThanOrEqual(1);
      expect(response.shift()).toBeInstanceOf(Product);
    });
  });

  /**
   * @see PublicService.getInstrument
   */
  describe('GetInstrument', () => {
    it('Positive', async () => {
      const response = await publicService.getInstrument({ instrumentId: 3 });
      expect(response).toBeInstanceOf(Instrument);
    });

    it('Negative', async () => {
      await asyncExpectThrow(async () => publicService.getInstrument({instrumentId: 33333}), AlphaPointNotFoundException);
    });
  });

  /**
   * @see PublicService.getInstruments
   */
  describe('GetInstruments', () => {
    it('Positive', async () => {
      const response = await publicService.getInstruments();
      expect(response).toBeInstanceOf(Array);
      expect(response.length).toBeGreaterThanOrEqual(1);
      expect(response.shift()).toBeInstanceOf(Instrument);
    });
  });

  /**
   * TODO: Return empty JSON payload. Marked as deprecated.
   *
   * @see PublicService.getTickerHistory
   */
  describe('GetTickerHistory', () => {
    it('Positive', async () => {
      await asyncExpectThrow(async () => publicService.getTickerHistory({instrumentId: 3, fromDate: 1568581012785}), EmptyResponseException);
    });

    it('Negative: Not found', async () => {
      await asyncExpectThrow(async () => publicService.getTickerHistory({instrumentId: 323323, fromDate: 1568581012785}), EmptyResponseException);
    });
  });

  /**
   * @see PrivateService.resetPassword
   */
  describe('ResetPassword', () => {
    it('Negative: Email is not verified', async () => {
      await asyncExpectThrow(async () => {
        return  publicService.resetPassword({username: 'test_2019-08-03_11-33-42', reCaptcha: '11232434'});
      }, AlphaPointNotFoundException);
    });
  });

  /**
   * @see PublicService.getLevel1
   */
  describe('GetLevel1', () => {
    it('Positive', async () => {
      const response = await publicService.getLevel1({instrumentId: 3});
      expect(response).toBeInstanceOf(Level1);
    });

    it('Negative', async () => {
      const response = await publicService.getLevel1({instrumentId: 435345555});
      expect(response).toBeInstanceOf(Level1);
    });
  });

  /**
   * @see PublicService.getL2Snapshot
   */
  describe('GetL2Snapshot', () => {
    it('Positive', async () => {
      const response = await publicService.getL2Snapshot({ instrumentId: 3});
      expect(response).toBeInstanceOf(Array);
    });
  });

  /**
   * @see PublicService.subscribeLevel1
   * @see PublicService.unsubscribeLevel1
   */
  describe('SubscribeTicker', () => {
    it('Subscribe to multiple subscriptions', async () => {
      await publicService.subscribeLevel1({instrumentId: 3}, (data: Level1EventData) => data);
      await publicService.subscribeLevel1({instrumentId: 1}, (data: Level1EventData) => data);
      // await publicService.subscribeLevel1({instrumentId: 1}, (data: Level1EventData) => data);
    });

    it('Unsubscribe', async () => {
      await publicService.unsubscribeLevel1({instrumentId: 3});
    });
  });

  /**
   * @see PublicService.subscribeLevel2
   * @see PublicService.unsubscribeLevel2
   */
  describe('SubscribeTicker', () => {
    it('Subscribe', async () => {
      await publicService.subscribeLevel2({instrumentId: 3}, (data: number[][]) => data);
    });

    it('Unsubscribe', async () => {
      await publicService.unsubscribeLevel2({instrumentId: 3});
    });
  });

  /**
   * @see PublicService.subscribeTicker
   * @see PublicService.unsubscribeTicker
   */
  describe('SubscribeTicker', () => {
    it('Subscribe', async () => {
      await publicService.subscribeTicker({instrumentId: 3, interval: 60}, (message: number[]) => message);
    });

    it('Unsubscribe', async () => {
      await publicService.unsubscribeTicker({instrumentId: 3});
    });
  });

  /**
   * @see PublicService.subscribeTrades
   * @see PublicService.unsubscribeTrades
   */
  describe('SubscribeTicker', () => {
    it('Subscribe', async () => {
      await publicService.subscribeTrades({instrumentId: 2, includeLastCount: 5}, (message: number[]) => message);
    });

    it('Unsubscribe', async () => {
      await publicService.unsubscribeTrades({instrumentId: 3});
    });
  });

  /**
   * @see PrivateService.logout
   */
  describe('LogOut', () => {
    it('Positive', async () => {
      await publicService.logout();
    });
  });
});
