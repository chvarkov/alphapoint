const path = require('path');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const nodeExternals = require('webpack-node-externals');

module.exports = {
    mode: 'production',
    entry: ['./src/main.ts'],
    externals: [nodeExternals()],
    target: 'node',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
        plugins: [new TsconfigPathsPlugin({ configFile: './tsconfig.json' })]
    },
    plugins: [
        new CopyPlugin([
            {
                from: 'src/**/*.hbs',
            },
            {
                from: 'env/**/*.env',
                ignore: ['*.dist'],
            },
        ]),
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'server.js',
    },
    optimization: {
        removeAvailableModules: true,
        removeEmptyChunks: true,
    }
};
