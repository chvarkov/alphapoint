export enum EnvironmentsEnum {
  Prod = 'production',
  Dev = 'development',
  Test = 'test',
}
