import { Injectable } from '@nestjs/common';
import * as nodemailer from 'nodemailer';
import { ConfigService } from '@core/services/config.service';
import * as Mail from 'nodemailer/lib/mailer';

@Injectable()
export class MailerService {
  private transporter: Mail;
  constructor(private readonly configService: ConfigService) {
    const { host, port, username, password } = this.configService.mailer;
    this.transporter = nodemailer.createTransport({
      host,
      port,
      auth: {
        user: username,
        pass: password,
      },
    });
  }

  async sendMail(options: Mail.Options): Promise<void> {
    await this.transporter.sendMail(options);
  }
}
