import { Injectable } from '@nestjs/common';
import { Request } from 'express';
import { ConfigService } from '@core/services/config.service';
import { MailerService } from '@core/services/mailer.service';
import * as fs from 'fs';
import * as Handlebars from 'handlebars';

@Injectable()
export class ErrorReporterService {
  constructor(private readonly configService: ConfigService, private readonly mailerService: MailerService) {}

  async report(request: Request, exception: Error): Promise<void> {
    const html = this.generateReport(request, exception);

    await this.mailerService.sendMail({
      from: '"NDAX error reporter👻" <chvarkov.alexey@gmail.com>',
      to: this.configService.errorRecipients,
      subject: 'Critical error on NDAX',
      html,
    });
  }

  generateReport(request: Request, exception: Error): string {
    const template = fs.readFileSync('@core/templates/error-report.hbs', 'utf8');
    const compileTemplate = Handlebars.compile(template);

    const stackTrace = exception.stack.split('\n');
    stackTrace.shift();

    const date = new Date();
    const twoChars = (value: number): string => (value > 9 ? value.toString() : `0${value}`);

    if (request.headers.authorization) {
      request.headers.authorization = 'Bearer *******';
    }

    return compileTemplate({
      request,
      exception,
      stackTrace,
      additionally: {
        date: `${twoChars(date.getDay())}.${twoChars(date.getMonth())}.${twoChars(date.getFullYear())}`,
        time: `${twoChars(date.getHours())}:${twoChars(date.getMinutes())}:${twoChars(date.getSeconds())}`,
        bodyJson: JSON.stringify(request.body, undefined, 4),
        exceptionType: exception.constructor.name,
      },
    });
  }
}
