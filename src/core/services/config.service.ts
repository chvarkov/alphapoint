import * as dotenv from 'dotenv';
import * as path from 'path';
import { Injectable } from '@nestjs/common';
import { EnvironmentsEnum } from '@core/enums/environments.enum';
import { IConnectionOptions } from '@alpha-point/interfaces/options.interfaces';

@Injectable()
export class ConfigService {
  private env = process.env;

  constructor() {
    const envPath = path.join('env', `${process.env.NODE_ENV || EnvironmentsEnum.Dev}.env`);
    dotenv.config({ path: envPath });
  }

  get isProduction(): boolean {
    return this.env.NODE_ENV === EnvironmentsEnum.Prod;
  }

  get mailer(): any {
    return {
      host: this.env.MAILER_HOST,
      port: this.env.MAILER_PORT,
      username: this.env.MAILER_USERNAME,
      password: this.env.MAILER_PASSWORD,
    };
  }

  get errorRecipients(): string {
    return this.env.ERROR_RECIPIENTS;
  }

  get alphaPointPublicConnectionOptions(): IConnectionOptions {
    return {
      wss: this.env.AP_WSS_PUBLIC,
      options: {},
      omsId: +this.env.AP_OMS_ID,
      attemptCount: +this.env.AP_ATTEMPT_COUNT,
      reconnectTimeout: +this.env.AP_RECONNECTION_TIMEOUT,
    };
  }

  get alphaPointPrivateConnectionOptions(): IConnectionOptions {
    return {
      wss: this.env.AP_WSS_PRIVATE,
      options: {},
      credentials: {
        username: this.env.AP_WSS_PRIVATE_USERNAME,
        password: this.env.AP_WSS_PRIVATE_PASSWORD,
      },
      omsId: +this.env.AP_OMS_ID,
      attemptCount: +this.env.AP_ATTEMPT_COUNT,
      reconnectTimeout: +this.env.AP_RECONNECTION_TIMEOUT,
    };
  }
}
