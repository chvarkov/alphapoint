import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  InternalServerErrorException,
  Injectable,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { ConfigService } from '@core/services/config.service';
import { ErrorReporterService } from '@core/services/error-reporter.service';

@Catch(Error)
@Injectable()
export class HttpExceptionFilter implements ExceptionFilter {
  constructor(private readonly configService: ConfigService, private readonly errorReporter: ErrorReporterService) {}

  async catch(exception: Error, host: ArgumentsHost): Promise<void> {
    const context = host.switchToHttp();
    const request = context.getRequest<Request>();

    const isCriticalError = exception instanceof InternalServerErrorException || !(exception instanceof HttpException);

    if (isCriticalError && this.configService.isProduction) {
      await this.errorReporter.report(request, exception);
    }

    const message = exception instanceof HttpException ? exception.message.message : exception.message;
    const statusCode = exception instanceof HttpException ? exception.message.statusCode : 500;

    const response = context.getResponse<Response>();

    response.status(statusCode).json({
      statusCode,
      message,
      timestamp: new Date().toISOString(),
      path: request.url,
    });
  }
}
