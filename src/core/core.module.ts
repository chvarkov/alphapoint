import { Global, Module } from '@nestjs/common';
import { ConfigService } from '@core/services/config.service';
import { ErrorReporterService } from '@core/services/error-reporter.service';
import { MailerService } from '@core/services/mailer.service';
import { HttpExceptionFilter } from '@core/filters/http-exception-filter';

@Global()
@Module({
  providers: [ConfigService, ErrorReporterService, MailerService, HttpExceptionFilter],
  exports: [ConfigService, ErrorReporterService, MailerService, HttpExceptionFilter],
})
export class CoreModule {}
