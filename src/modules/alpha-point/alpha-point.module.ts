import { DynamicModule, Module, Provider } from '@nestjs/common';
import { PublicService } from './services/public.service';
import { PrivateService } from './services/private.service';
import { Connection } from './services/connection';
import { IModuleOptions } from './interfaces/options.interfaces';
import { WebSocketFactory } from '@alpha-point/services/web-socket.factory';

export const AP_PUBLIC_CONNECTION = 'AP_PUBLIC_CONNECTION';
export const AP_PRIVATE_CONNECTION = 'AP_PRIVATE_CONNECTION';

const EXPORTS = [PublicService, PrivateService];

@Module({
  exports: EXPORTS,
})
export class AlphaPointModule {
  static forRoot(options: IModuleOptions): DynamicModule {
    const providers: Provider[] = [
      WebSocketFactory,
      PublicService,
      PrivateService,
      {
        name: AP_PUBLIC_CONNECTION,
        provide: AP_PUBLIC_CONNECTION,
        inject: [WebSocketFactory],
        useFactory: async (wsFactory: WebSocketFactory): Promise<Connection> => {
          return await new Connection(options.public, wsFactory).connect();
        },
      },
      {
        name: AP_PUBLIC_CONNECTION,
        provide: AP_PRIVATE_CONNECTION,
        inject: [WebSocketFactory],
        useFactory: async (wsFactory: WebSocketFactory): Promise<Connection> => {
          return await new Connection(options.private, wsFactory).connect();
        },
      },
    ];

    return {
      module: AlphaPointModule,
      providers,
      exports: [...EXPORTS, ...providers],
    };
  }
}
