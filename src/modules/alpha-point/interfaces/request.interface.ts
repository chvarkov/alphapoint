import { ITypingOptions } from 'src/modules/alpha-point/interfaces/options.interfaces';
import { EndpointType, SnapshotEventCallback, SubscriptionCallback } from 'src/modules/alpha-point/services/connection';
import { EventsEnum } from 'src/modules/alpha-point/enums/events.enum';
import { IMessage } from 'src/modules/alpha-point/interfaces/message.interface';

export interface IRequest<I, O> extends ITypingOptions<I, O> {
  method: EndpointType;
  data: I;
}

export interface ISubscriptionRequest<I, O> extends IRequest<I, O> {
  event: EventsEnum;
  callback: SubscriptionCallback<O>;
  snapshot: string;
  snapshotCallback: SnapshotEventCallback<O>;
}
