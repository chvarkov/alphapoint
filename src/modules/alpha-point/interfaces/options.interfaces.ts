import * as WebSocket from 'ws';
import { ClassType } from 'class-transformer/ClassTransformer';
import { PublicSubscriptionsEnum } from 'src/modules/alpha-point/enums/common/public-subscriptions.enum';
import { EndpointType, SnapshotEventCallback, SubscriptionCallback } from 'src/modules/alpha-point/services/connection';
import { IMap } from 'src/modules/alpha-point/interfaces/map.interface';
import { EventsEnum } from '../enums/events.enum';

export interface IModuleOptions {
  public: IConnectionOptions;
  private: IConnectionOptions;
}

export interface IConnectionOptions {
  wss: string;
  omsId: number;
  credentials?: ICredentials;
  options?: WebSocket.ClientOptions;
  attemptCount?: number;
  reconnectTimeout?: number;
}

export interface ICredentials {
  username: string;
  password: string;
}

export interface ITypingRequest<T> {
  request: ClassType<T>;
}

export interface ITypingResponse<T> {
  response: ClassType<T>;
}

export interface ITypingOptions<I, O> extends ITypingRequest<I>, ITypingResponse<O> {}

export interface IRequestOptions<I, O> {
  resolve: any;
  reject: any;
  types: ITypingOptions<I, O>;
}

export interface ISubscriptionOptions<I, O> extends ITypingOptions<I, O> {
  event: EventsEnum;
  callbacks: IMap<SubscriptionCallback<O | O[]>>;
  snapshot: string;
  snapshotCallback: SnapshotEventCallback<O>;
}

export interface ISubscriptionsMap<I, O> extends IMap<ISubscriptionOptions<I, O>> {}
