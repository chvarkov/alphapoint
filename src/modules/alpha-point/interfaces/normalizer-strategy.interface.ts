export interface INormalizerStrategy<T> extends INormalize<T>, IDenormalize<T> {}

export interface INormalize<T> {
  normalize(array: any[]): T;

  normalizeArray(twoDimensionalArray: any[][]): T[];
}

export interface IDenormalize<T> {
  denormalize(model: T): any[];

  denormalizeArray(modelArray: T[]): any[][];
}
