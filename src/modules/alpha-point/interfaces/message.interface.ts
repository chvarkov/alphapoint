import { MessageTypeEnum } from 'src/modules/alpha-point/enums/message-type.enum';
import { PrivateMethodsEnum } from 'src/modules/alpha-point/enums/common/private-methods.enum';
import { PublicMethodsEnum } from 'src/modules/alpha-point/enums/common/public-methods.enum';
import { ClassType } from 'class-transformer/ClassTransformer';
import { EndpointType } from '../services/connection';

export interface IMessage<T> {
  type: MessageTypeEnum;

  index: number;

  method: EndpointType;

  payloadJson: string;

  getPayload(payloadClass?: ClassType<T>): T | T[];
}
