import { IRequestOptions, ISubscriptionsMap } from 'src/modules/alpha-point/interfaces/options.interfaces';
import { IMessage } from 'src/modules/alpha-point/interfaces/message.interface';

export interface IAcceptionContext {
  requests: Array<IRequestOptions<any, any>>;
  subscriptions: ISubscriptionsMap<any, any>;
}

export interface IAcceptorStrategy {
  accept(message: IMessage<any>): void;
}
