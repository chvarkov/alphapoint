import { NotFoundException } from '@nestjs/common';
import { Result } from 'src/modules/alpha-point/models/message';

export class AlphaPointNotFoundException extends NotFoundException {
  constructor(result: Result) {
    super(`${result.errorMessage}:\n${result.detail}`);
  }
}
