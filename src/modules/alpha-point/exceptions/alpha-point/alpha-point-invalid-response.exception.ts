import { BadRequestException } from '@nestjs/common';
import { Result } from 'src/modules/alpha-point/models/message';

export class AlphaPointInvalidResponseException extends BadRequestException {
  constructor(result: Result) {
    super(`${result.errorMessage}:\n${result.detail}`);
  }
}
