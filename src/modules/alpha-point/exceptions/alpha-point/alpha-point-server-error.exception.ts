import { BadGatewayException } from '@nestjs/common';
import { Result } from 'src/modules/alpha-point/models/message';

export class AlphaPointServerErrorException extends BadGatewayException {
  constructor(result: Result) {
    super(`${result.errorMessage}:\n${result.detail}`);
  }
}
