import { InternalServerErrorException } from '@nestjs/common';

export class FailedAuthenticationException extends InternalServerErrorException {
  constructor(message?: string | object | any, error?: string) {
    super(message || `Failed authentication to private connection.`, error);
  }
}
