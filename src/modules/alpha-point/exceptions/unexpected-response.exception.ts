import { InternalServerErrorException } from '@nestjs/common';

export class UnexpectedResponseException extends InternalServerErrorException {
  constructor(payload: any, message?: string | object | any, error?: string) {
    super(message || `Unexpected response from AlphaPoint: \n${JSON.stringify(payload)}`, error);
  }
}
