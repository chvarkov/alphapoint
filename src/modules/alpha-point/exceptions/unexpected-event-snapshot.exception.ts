import { InternalServerErrorException } from '@nestjs/common';

export class UnexpectedEventSnapshotException extends InternalServerErrorException {
  constructor(snapshot: string, message?: string | object | any, error?: string) {
    super(message || `Unexpected event snapshot: ${snapshot}`, error);
  }
}
