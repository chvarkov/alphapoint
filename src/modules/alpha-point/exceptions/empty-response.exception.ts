import { BadGatewayException } from '@nestjs/common';

export class EmptyResponseException extends BadGatewayException {
  constructor(message?: string | object | any, error?: string) {
    super(message || 'Empty response from AlphaPoint.', error);
  }
}
