import { Connection } from './connection';
import { UnexpectedResponseException } from '../exceptions/unexpected-response.exception';
import { IRequest, ISubscriptionRequest } from 'src/modules/alpha-point/interfaces/request.interface';
import { RequestBuilder } from './builders/request.builder';
import { PublicMethodsEnum } from '../enums/common/public-methods.enum';
import { ResetPasswordRequest } from '@alpha-point/models/users';

export class AbstractService {
  constructor(protected connection: Connection) {}

  async resetPassword(data: ResetPasswordRequest): Promise<void> {
    const request = new RequestBuilder()
      .setMethod(PublicMethodsEnum.ResetPassword)
      .setData(data)
      .build();

    await this.connection.send(request);
  }

  async logout(): Promise<void> {
    const request = new RequestBuilder().setMethod(PublicMethodsEnum.LogOut).build();

    await this.connection.send(request);
  }

  protected async fetchOne<I, O>(request: IRequest<I, O>): Promise<O> {
    const payload = await this.connection.send(request);
    return this.getOne(payload);
  }

  protected async fetchList<I, O>(request: IRequest<I, O>): Promise<O[]> {
    const payload = await this.connection.send(request);
    return this.getList(payload);
  }

  protected getEventItem<T>(value: T | T[]): T {
    return Array.isArray(value) ? value.shift() : value;
  }

  protected async subscribeAndFetchOne<I, O>(request: ISubscriptionRequest<I, O>): Promise<O> {
    const payload = await this.connection.addSubscription(request);
    return this.getOne(payload);
  }

  protected async subscribeAndFetchList<I, O>(request: ISubscriptionRequest<I, O>): Promise<O[]> {
    const payload = await this.connection.addSubscription(request);
    return this.getList(payload);
  }

  private getOne<T>(value: T | T[]): T {
    if (!Array.isArray(value)) {
      return value;
    }
    throw new UnexpectedResponseException(value);
  }

  private getList<T>(value: T | T[]): T[] {
    if (Array.isArray(value)) {
      return value;
    }
    throw new UnexpectedResponseException(value);
  }
}
