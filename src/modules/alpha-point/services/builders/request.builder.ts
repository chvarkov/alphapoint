import { EndpointType } from 'src/modules/alpha-point/services/connection';
import { IRequest } from 'src/modules/alpha-point/interfaces/request.interface';
import { InternalServerErrorException } from '@nestjs/common';

export class RequestBuilder<I, O> {
  protected data?: I = null;
  protected method: EndpointType;

  constructor(protected request: new () => I = null, protected response: new () => O = null) {}

  setMethod(method: EndpointType): this {
    this.method = method;

    return this;
  }

  setData(data: I): this {
    this.data = this.request ? Object.assign(new this.request(), data) : data;

    return this;
  }

  build(): IRequest<I, O> {
    if (!this.method) {
      throw new InternalServerErrorException('Failed build request.');
    }

    return {
      method: this.method,
      data: this.data,
      request: this.request,
      response: this.response,
    };
  }
}
