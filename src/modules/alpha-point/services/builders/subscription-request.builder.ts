import { ISubscriptionRequest } from '../../interfaces/request.interface';
import { RequestBuilder } from './request.builder';
import { EventsEnum } from '../../enums/events.enum';
import { SnapshotEventCallback, SubscriptionCallback } from '../connection';
import { InternalServerErrorException } from '@nestjs/common';

export class SubscriptionRequestBuilder<I, O> extends RequestBuilder<I, O> {
  event: EventsEnum;
  callback: SubscriptionCallback<O>;
  snapshot: string;
  snapshotCallback: SnapshotEventCallback<O>;

  constructor(request: new () => I = null, response: new () => O = null) {
    super(request, response);
  }

  setCallback(callback: SubscriptionCallback<O>): this {
    this.callback = callback;

    return this;
  }

  setEvent(event: EventsEnum): this {
    this.event = event;

    return this;
  }

  setSnapshot(snapshot: string): this {
    this.snapshot = snapshot;

    return this;
  }

  setSnapshotCallback(snapshotCallback: SnapshotEventCallback<O | O[]>): this {
    this.snapshotCallback = snapshotCallback;

    return this;
  }

  build(): ISubscriptionRequest<I, O> {
    if (!this.event || !this.callback || !this.snapshot || !this.snapshotCallback) {
      throw new InternalServerErrorException('Failed build subscription request.');
    }

    return {
      ...super.build(),
      event: this.event,
      callback: this.callback,
      snapshot: this.snapshot,
      snapshotCallback: this.snapshotCallback,
    };
  }
}
