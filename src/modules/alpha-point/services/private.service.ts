import { Inject, Injectable } from '@nestjs/common';
import { PrivateMethodsEnum } from '../enums/common/private-methods.enum';
import { Connection } from './connection';
import { AbstractService } from './abstract-service';
import { RequestBuilder } from './builders/request.builder';
import {
  AccountInfo,
  AccountInfoRequest,
  AccountPositionItem,
  AccountPositionsRequest,
} from '@alpha-point/models/accounts';
import { AccountTrade, AccountTradesRequest } from '@alpha-point/models/trades';
import {
  AccountTransactionsRequest,
  AllWithdrawTicketsRequest,
  DepositTicket,
  DepositTicketsRequest,
  Transaction,
  WithdrawTicket,
} from '@alpha-point/models/tickets';
import {
  Authenticate2FA,
  Authenticate2FARequest,
  ForceUserLogoffRequest,
  LoggedInUserRequest,
  RegisterUserInfo,
  ResendVerificationEmailRequest,
  SetUserConfigRequest,
  SetVerificationLevelRequest,
  UserAccountsRequest,
  UserAffiliateCount,
  UserAffiliateCountRequest,
  UserConfig,
  UserConfigItem,
  UserConfigRequest,
  UserInfo,
  UserInfoRequest,
  UserPermissionsRequest,
  UserRegisterRequest,
} from '@alpha-point/models/users';
import { AP_PRIVATE_CONNECTION } from '@alpha-point/alpha-point.module';

@Injectable()
export class PrivateService extends AbstractService {
  constructor(@Inject('AP_PRIVATE_CONNECTION') protected readonly connection: Connection) {
    super(connection);
  }

  async registerUser(data: UserRegisterRequest): Promise<void> {
    data.info = new RegisterUserInfo(data.info);

    const request = new RequestBuilder(UserRegisterRequest)
      .setMethod(PrivateMethodsEnum.RegisterUser)
      .setData(data)
      .build();

    await this.connection.send(request);
  }

  async authenticate2fa(data: Authenticate2FARequest): Promise<Authenticate2FA> {
    const request = new RequestBuilder(Authenticate2FARequest, Authenticate2FA)
      .setMethod(PrivateMethodsEnum.Authenticate2FA)
      .setData(data)
      .build();

    return this.fetchOne(request);
  }

  async getLoggedInUserBySessionToken(data: LoggedInUserRequest): Promise<any> {
    // TODO: Create model for response
    const request = new RequestBuilder(LoggedInUserRequest)
      .setMethod(PrivateMethodsEnum.GetLoggedInUserBySessionToken)
      .setData(data)
      .build();

    return this.connection.send(request);
  }

  async forceUserLogoff(data: ForceUserLogoffRequest): Promise<void> {
    const request = new RequestBuilder()
      .setMethod(PrivateMethodsEnum.ForceUserLogoff)
      .setData(data)
      .build();

    await this.connection.send(request);
  }

  async resendVerificationEmail(data: ResendVerificationEmailRequest): Promise<void> {
    const request = new RequestBuilder()
      .setMethod(PrivateMethodsEnum.ResendVerificationEmail)
      .setData(data)
      .build();

    await this.connection.send(request);
  }

  async setVerificationLevel(data: SetVerificationLevelRequest): Promise<void> {
    const request = new RequestBuilder(SetVerificationLevelRequest)
      .setMethod(PrivateMethodsEnum.SetUserVerificationLevel)
      .setData(data)
      .build();

    await this.connection.send(request);
  }

  async getAvailablePermissionList(): Promise<string[]> {
    const request = new RequestBuilder<null, string>().setMethod(PrivateMethodsEnum.GetAvailablePermissionList).build();

    return await this.fetchList(request);
  }

  async getAccountTrades(data: AccountTradesRequest): Promise<AccountTrade[]> {
    const request = new RequestBuilder(AccountTradesRequest, AccountTrade)
      .setMethod(PrivateMethodsEnum.GetAccountTrades)
      .setData(data)
      .build();

    return this.fetchList(request);
  }

  async getAllWithdrawTickets(data: AllWithdrawTicketsRequest): Promise<WithdrawTicket[]> {
    const request = new RequestBuilder(AllWithdrawTicketsRequest, WithdrawTicket)
      .setMethod(PrivateMethodsEnum.GetAllWithdrawTickets)
      .setData(data)
      .build();

    return this.fetchList(request);
  }

  async getUserInfo(data: UserInfoRequest): Promise<UserInfo> {
    const request = new RequestBuilder(UserInfoRequest, UserInfo)
      .setMethod(PrivateMethodsEnum.GetUserInfo)
      .setData(data)
      .build();

    return await this.fetchOne(request);
  }

  async getAccountInfo(data: AccountInfoRequest): Promise<AccountInfo> {
    const request = new RequestBuilder(AccountInfoRequest, AccountInfo)
      .setMethod(PrivateMethodsEnum.GetAccountInfo)
      .setData(data)
      .build();

    return await this.fetchOne(request);
  }

  async getAccountPositions(data: AccountPositionsRequest): Promise<AccountPositionItem[]> {
    const request = new RequestBuilder(AccountPositionsRequest, AccountPositionItem)
      .setMethod(PrivateMethodsEnum.GetAccountPositions)
      .setData(data)
      .build();

    return await this.fetchList(request);
  }

  async getAccountWithdrawTransactions(data: AccountTransactionsRequest): Promise<Transaction[]> {
    const request = new RequestBuilder(AccountTransactionsRequest, Transaction)
      .setMethod(PrivateMethodsEnum.GetAccountWithdrawTransactions)
      .setData(data)
      .build();

    return this.fetchList(request);
  }

  async getAccountDepositTransactions(data: AccountTransactionsRequest): Promise<Transaction[]> {
    const request = new RequestBuilder(AccountTransactionsRequest, Transaction)
      .setMethod(PrivateMethodsEnum.GetAccountDepositTransactions)
      .setData(data)
      .build();

    return this.fetchList(request);
  }

  async getAllDepositTickets(data: DepositTicketsRequest): Promise<DepositTicket[]> {
    const request = new RequestBuilder(DepositTicketsRequest, DepositTicket)
      .setMethod(PrivateMethodsEnum.GetAllDepositTickets)
      .setData(data)
      .build();

    return this.fetchList(request);
  }

  async getUserConfig(data: UserConfigRequest): Promise<UserConfig> {
    const request = new RequestBuilder(UserConfigRequest, UserConfigItem)
      .setMethod(PrivateMethodsEnum.GetUserConfig)
      .setData(data)
      .build();

    const configs = await this.fetchList(request);

    return new UserConfig(configs);
  }

  async setUserConfig(data: SetUserConfigRequest): Promise<void> {
    const request = new RequestBuilder<SetUserConfigRequest, void>(SetUserConfigRequest)
      .setMethod(PrivateMethodsEnum.SetUserConfig)
      .setData(data)
      .build();

    await this.connection.send(request);
  }

  async getAllUnredactedUserConfigsForUser(data: UserConfigRequest): Promise<UserConfig> {
    const request = new RequestBuilder(UserConfigRequest, UserConfigItem)
      .setMethod(PrivateMethodsEnum.GetAllUnredactedUserConfigsForUser)
      .setData(data)
      .build();

    const configs = await this.fetchList(request);

    return new UserConfig(configs);
  }

  async getUserPermissions(data: UserPermissionsRequest): Promise<string[]> {
    const request = new RequestBuilder<UserPermissionsRequest, string>(UserPermissionsRequest)
      .setMethod(PrivateMethodsEnum.GetUserPermissions)
      .setData(data)
      .build();

    return await this.fetchList(request);
  }

  async getUserAccounts(data: UserAccountsRequest): Promise<number[]> {
    const request = new RequestBuilder<UserAccountsRequest, number>(UserAccountsRequest)
      .setMethod(PrivateMethodsEnum.GetUserAccounts)
      .setData(data)
      .build();

    return await this.fetchList(request);
  }

  async getUserAffiliateCount(data: UserAffiliateCountRequest): Promise<UserAffiliateCount> {
    const request = new RequestBuilder(UserAffiliateCountRequest, UserAffiliateCount)
      .setMethod(PrivateMethodsEnum.GetUserAffiliateCount)
      .setData(data)
      .build();

    return await this.fetchOne(request);
  }
}
