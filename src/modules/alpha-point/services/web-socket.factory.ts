import * as WebSocket from 'ws';

export class WebSocketFactory {
  createClient(wss: string, options: WebSocket.ClientOptions): WebSocket {
    return new WebSocket(wss, options);
  }
}
