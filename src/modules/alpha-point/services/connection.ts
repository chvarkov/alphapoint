import { BadGatewayException, Injectable, Logger } from '@nestjs/common';
import * as WebSocket from 'ws';
import { Message, OmsOption } from '../models/message';
import { deserialize, serialize } from 'class-transformer';
import { PublicMethodsEnum } from '../enums/common/public-methods.enum';
import { PrivateMethodsEnum } from '../enums/common/private-methods.enum';
import { Credentials, WebAuthenticateUser } from '../models/users';
import { IConnectionOptions, IRequestOptions, ISubscriptionsMap } from '../interfaces/options.interfaces';
import { MessageTypeEnum } from '../enums/message-type.enum';
import { AcceptorFactory } from './acceptor/acceptor.factory';
import { IAcceptionContext } from '../interfaces/acceptor-strategy.interface';
import { PublicSubscriptionsEnum } from '../enums/common/public-subscriptions.enum';
import { PublicUnsubscriptionsEnum } from '../enums/common/public-unsubscriptions.enum';
import { IRequest, ISubscriptionRequest } from '../interfaces/request.interface';
import { RequestBuilder } from './builders/request.builder';
import { IMessage } from '../interfaces/message.interface';
import { UnexpectedResponseException } from '../exceptions/unexpected-response.exception';
import { EventsEnum } from '../enums/events.enum';
import { FailedAuthenticationException } from '../exceptions/failed-authentication.exception';
import { WebSocketFactory } from '@alpha-point/services/web-socket.factory';

export type EndpointType = MethodType | SubscriptionType | UnsubscriptionType | EventsEnum;
export type MethodType = PublicMethodsEnum | PrivateMethodsEnum;
export type SubscriptionType = PublicSubscriptionsEnum;
export type UnsubscriptionType = PublicUnsubscriptionsEnum;

export type SubscriptionCallback<T> = (data: T | T[]) => void;
export type SnapshotEventCallback<T> = (message: IMessage<T>) => string;

@Injectable()
export class Connection {
  private readonly STEP_INDEX = 2;

  private requestIndex = 0;

  private requests: Array<IRequestOptions<any, any>> = [];
  private subscriptions: ISubscriptionsMap<any, any> = {};

  private wssClient: WebSocket;

  private currentAttemptCount = 0;

  constructor(private config: IConnectionOptions, private webSocketFactory: WebSocketFactory) {
  }

  send<I, O>(request: IRequest<I, O>): Promise<O | O[]> {
    return new Promise<O | O[]>((resolve, reject) => {
      this.requestIndex += this.STEP_INDEX;

      if (request.data instanceof OmsOption) {
        (request.data as OmsOption).omsId = this.config.omsId;
      }

      const message = Object.assign(new Message<I>(), {
        type: MessageTypeEnum.Request,
        index: this.requestIndex,
        method: request.method,
        payloadJson: serialize<I>(request.data),
      });

      this.wssClient.send(serialize(message), err => {
        if (err) {
          reject(new BadGatewayException(err.message));
          return;
        }

        this.addRequest({ resolve, reject, types: request });
      });
    });
  }

  async addSubscription<I, O>(subscription: ISubscriptionRequest<I, O>): Promise<O | O[]> {
    const result = await this.send<I, O>(subscription);

    const event = subscription.event;
    const snapshot = subscription.snapshot;

    if (!this.subscriptions[event]) {
      const { request, response, snapshotCallback } = subscription;
      this.subscriptions[event] = {
        event,
        request,
        response,
        snapshot,
        snapshotCallback,
        callbacks: {},
      };
    }

    this.subscriptions[event].callbacks[snapshot] = subscription.callback;

    return result;
  }

  connect(): Promise<Connection> {
    return new Promise<Connection>(async (resolve, reject) => {
      this.wssClient = this.webSocketFactory.createClient(this.config.wss, this.config.options);

      this.wssClient.onmessage = (message: WebSocket.MessageEvent) => this.accept(message);

      this.wssClient.onopen = async () => {
        try {
          if (this.config.credentials) {
            await this.authenticate();
          }

          Logger.log(this.config.credentials ? 'Connected to private connection' : 'Connected to public connection');

          resolve(this);
        } catch (e) {
          reject(e);
        }
      };

      this.wssClient.onerror = async event => {
        if (this.currentAttemptCount++ === this.config.attemptCount) {
          reject(new BadGatewayException(`Unable connect to WSS: ${event.message}`));
          return;
        }

        setTimeout(async () => {
          try {
            resolve(await this.connect());
          } catch (e) {
            reject(e);
          }
        }, this.config.reconnectTimeout);
      };
    });
  }

  async accept(event: WebSocket.MessageEvent): Promise<void> {
    const message = deserialize(Message, event.data.toString());

    if (this.isLogoutEvent(message)) {
      return await this.authenticate();
    }

    AcceptorFactory.create(message.type, this.getContext()).accept(message);
  }

  private async authenticate(): Promise<void> {
    const request = new RequestBuilder(Credentials, WebAuthenticateUser)
      .setMethod(PrivateMethodsEnum.WebAuthenticateUser)
      .setData(this.config.credentials)
      .build();

    const authResult = await this.send(request);

    if (!(authResult instanceof WebAuthenticateUser)) {
      throw new UnexpectedResponseException(authResult);
    }

    if (!authResult.authenticated) {
      throw new FailedAuthenticationException();
    }
  }

  private isLogoutEvent(message: IMessage<any>) {
    return message.type === MessageTypeEnum.Event && message.method === EventsEnum.LogoutEvent;
  }

  private addRequest(request: IRequestOptions<any, any>): void {
    this.requests[this.requestIndex] = request;
  }

  private getContext(): IAcceptionContext {
    return {
      requests: this.requests,
      subscriptions: this.subscriptions,
    };
  }
}
