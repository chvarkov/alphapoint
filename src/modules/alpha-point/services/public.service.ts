import { Inject, Injectable } from '@nestjs/common';
import { Connection, SubscriptionCallback } from './connection';
import { PublicMethodsEnum } from '../enums/common/public-methods.enum';
import { InstrumentAwareRequest, OmsOption } from '../models/message';
import { AbstractService } from './abstract-service';
import { PublicSubscriptionsEnum } from '../enums/common/public-subscriptions.enum';
import { SubscribeTickerRequest } from '../models/subscribe-ticker';
import { SubscribeTradesRequest } from '../models/subscribe-trades';
import { TickerHistoryRequest } from '../models/ticker-history';
import { EventsEnum } from '../enums/events.enum';
import { RequestBuilder } from './builders/request.builder';
import { SubscriptionRequestBuilder } from './builders/subscription-request.builder';
import { IMessage } from '../interfaces/message.interface';
import { normalize } from '../helpers/normalizer.helper';
import { Product, ProductRequest } from '@alpha-point/models/products';
import { Instrument, InstrumentRequest } from '@alpha-point/models/instruments';
import { L2Snapshot, L2SnapshotRequest, Level1, Level1EventData } from '@alpha-point/models/users';
import { Level2EventData, Ticker, Trade } from '@alpha-point/models/normalized';
import { AP_PUBLIC_CONNECTION } from '@alpha-point/alpha-point.module';

@Injectable()
export class PublicService extends AbstractService {
  private eventSnapshotPrefix = 'InstrumentId';

  constructor(@Inject('AP_PUBLIC_CONNECTION') protected readonly connection: Connection) {
    super(connection);
  }

  async getProduct(data: ProductRequest): Promise<Product> {
    const request = new RequestBuilder(ProductRequest, Product)
      .setMethod(PublicMethodsEnum.GetProduct)
      .setData(data)
      .build();

    return this.fetchOne(request);
  }

  async getProducts(): Promise<Product[]> {
    const request = new RequestBuilder(OmsOption, Product)
      .setMethod(PublicMethodsEnum.GetProducts)
      .setData(new OmsOption())
      .build();

    return this.fetchList(request);
  }

  async getInstrument(data: InstrumentRequest): Promise<Instrument> {
    const request = new RequestBuilder(InstrumentRequest, Instrument)
      .setMethod(PublicMethodsEnum.GetInstrument)
      .setData(data)
      .build();

    return this.fetchOne(request);
  }

  async getInstruments(): Promise<Instrument[]> {
    const request = new RequestBuilder(OmsOption, Instrument)
      .setMethod(PublicMethodsEnum.GetInstruments)
      .setData(new OmsOption())
      .build();

    return this.fetchList(request);
  }

  async getLevel1(data: InstrumentAwareRequest): Promise<Level1> {
    const request = new RequestBuilder(InstrumentAwareRequest, Level1)
      .setMethod(PublicMethodsEnum.GetLevel1)
      .setData(data)
      .build();

    return this.fetchOne(request);
  }

  async getL2Snapshot(data: L2SnapshotRequest): Promise<L2Snapshot[]> {
    const request = new RequestBuilder(L2SnapshotRequest, L2Snapshot)
      .setMethod(PublicMethodsEnum.GetL2Snapshot)
      .setData(data)
      .build();

    return this.fetchList(request);
  }

  /**
   * @deprecated
   */
  async getTickerHistory(data: TickerHistoryRequest): Promise<number[][]> {
    const request = new RequestBuilder<TickerHistoryRequest, number[]>(TickerHistoryRequest)
      .setMethod(PublicMethodsEnum.GetTickerHistory)
      .setData(data)
      .build();

    return await this.fetchList(request);
  }

  async subscribeLevel1(
    data: InstrumentAwareRequest,
    callback: SubscriptionCallback<Level1EventData>,
  ): Promise<Level1EventData> {
    const snapshotCallback = (message: IMessage<Level1EventData>) => {
      const item = this.getEventItem(message.getPayload(Level1EventData));
      return `${this.eventSnapshotPrefix}:${item.instrumentId}`;
    };

    const request = new SubscriptionRequestBuilder(InstrumentAwareRequest, Level1EventData)
      .setMethod(PublicSubscriptionsEnum.SubscribeLevel1)
      .setData(data)
      .setEvent(EventsEnum.Level1UpdateEvent)
      .setCallback(callback)
      .setSnapshot(`${this.eventSnapshotPrefix}:${data.instrumentId}`)
      .setSnapshotCallback(snapshotCallback)
      .build();

    return await this.subscribeAndFetchOne(request);
  }

  async unsubscribeLevel1(data: InstrumentAwareRequest): Promise<void> {
    const request = new RequestBuilder(InstrumentAwareRequest)
      .setMethod(PublicMethodsEnum.UnsubscribeLevel1)
      .setData(data)
      .build();

    await this.connection.send(request);
  }

  async subscribeLevel2(data: L2SnapshotRequest, callback: SubscriptionCallback<number[]>): Promise<number[][]> {
    const snapshotCallback = (message: IMessage<number[][]>) => {
      const item = this.getEventItem<number[][]>(message.getPayload());
      const level2EventData = normalize(Level2EventData, item);
      return `${this.eventSnapshotPrefix}:${level2EventData.productPairCode}`;
    };

    const request = new SubscriptionRequestBuilder<L2SnapshotRequest, number[]>(L2SnapshotRequest)
      .setMethod(PublicSubscriptionsEnum.SubscribeLevel2)
      .setData(data)
      .setEvent(EventsEnum.Level2UpdateEvent)
      .setCallback(callback)
      .setSnapshot(`${this.eventSnapshotPrefix}:${data.instrumentId}`)
      .setSnapshotCallback(snapshotCallback)
      .build();

    return await this.subscribeAndFetchList(request);
  }

  async unsubscribeLevel2(data: InstrumentAwareRequest): Promise<void> {
    const request = new RequestBuilder(InstrumentAwareRequest)
      .setMethod(PublicMethodsEnum.UnsubscribeLevel2)
      .setData(data)
      .build();

    await this.connection.send(request);
  }

  async subscribeTicker(data: SubscribeTickerRequest, callback: SubscriptionCallback<number[]>): Promise<number[][]> {
    const snapshotCallback = (message: IMessage<number[][]>) => {
      const item = this.getEventItem<number[][]>(message.getPayload());
      const ticker = normalize(Ticker, item);
      return `${this.eventSnapshotPrefix}:${ticker.instrumentId}`;
    };

    const request = new SubscriptionRequestBuilder<SubscribeTickerRequest, number[]>(SubscribeTickerRequest)
      .setMethod(PublicSubscriptionsEnum.SubscribeTicker)
      .setData(data)
      .setEvent(EventsEnum.TickerDataUpdateEvent)
      .setCallback(callback)
      .setSnapshot(`${this.eventSnapshotPrefix}:${data.instrumentId}`)
      .setSnapshotCallback(snapshotCallback)
      .build();

    return await this.subscribeAndFetchList(request);
  }

  async unsubscribeTicker(data: InstrumentAwareRequest): Promise<void> {
    const request = new RequestBuilder(InstrumentAwareRequest)
      .setMethod(PublicMethodsEnum.UnsubscribeTicker)
      .setData(data)
      .build();

    await this.connection.send(request);
  }

  async subscribeTrades(data: SubscribeTradesRequest, callback: SubscriptionCallback<number[]>): Promise<number[][]> {
    const snapshotCallback = (message: IMessage<number[][]>) => {
      const item = this.getEventItem<number[][]>(message.getPayload());
      const ticker = normalize(Trade, item);
      return `${this.eventSnapshotPrefix}:${ticker.productPairCode}`;
    };

    const request = new SubscriptionRequestBuilder<SubscribeTradesRequest, number[]>(SubscribeTradesRequest)
      .setMethod(PublicSubscriptionsEnum.SubscribeTrades)
      .setData(data)
      .setEvent(EventsEnum.OrderTradeEvent)
      .setCallback(callback)
      .setSnapshot(`${this.eventSnapshotPrefix}:${data.instrumentId}`)
      .setSnapshotCallback(snapshotCallback)
      .build();

    return await this.subscribeAndFetchList(request);
  }

  async unsubscribeTrades(data: InstrumentAwareRequest): Promise<void> {
    const request = new RequestBuilder(InstrumentAwareRequest)
      .setMethod(PublicMethodsEnum.UnsubscribeTrades)
      .setData(data)
      .build();

    await this.connection.send(request);
  }
}
