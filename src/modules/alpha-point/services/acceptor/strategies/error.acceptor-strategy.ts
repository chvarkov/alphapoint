import { IAcceptionContext } from 'src/modules/alpha-point/interfaces/acceptor-strategy.interface';
import { IMessage } from 'src/modules/alpha-point/interfaces/message.interface';
import { BadGatewayException, Logger } from '@nestjs/common';
import { AbstractAcceptorStrategy } from '../abstract-acceptor-strategy';

export class ErrorAcceptorStrategy extends AbstractAcceptorStrategy {
  constructor(protected context: IAcceptionContext) {
    super(context);
  }

  accept(message: IMessage<any>): void {
    this.throwException(message.index, new BadGatewayException(message.payloadJson));
  }
}
