import { IAcceptionContext } from 'src/modules/alpha-point/interfaces/acceptor-strategy.interface';
import { IMessage } from 'src/modules/alpha-point/interfaces/message.interface';
import { AbstractAcceptorStrategy } from '../abstract-acceptor-strategy';
import { UnexpectedResponseException } from '../../../exceptions/unexpected-response.exception';
import { UnexpectedEventSnapshotException } from '../../../exceptions/unexpected-event-snapshot.exception';

export class EventAcceptorStrategy extends AbstractAcceptorStrategy {
  constructor(protected context: IAcceptionContext) {
    super(context);
  }

  accept(message: IMessage<any>): void {
    const subscription = this.context.subscriptions[message.method];

    if (!subscription) {
      this.throwException(message.index, new UnexpectedResponseException(message.index));
    }

    const snapshot = subscription.snapshotCallback(message);

    if (!subscription.callbacks[snapshot]) {
      this.throwException(message.index, new UnexpectedEventSnapshotException(snapshot));
    }

    const eventData = message.getPayload(subscription.response);

    subscription.callbacks[snapshot](eventData);
  }
}
