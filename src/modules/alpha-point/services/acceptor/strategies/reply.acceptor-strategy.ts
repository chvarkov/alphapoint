import { IAcceptionContext } from '../../../interfaces/acceptor-strategy.interface';
import { IMessage } from '../../../interfaces/message.interface';
import { Result } from '../../../models/message';
import { UnexpectedResponseException } from '../../../exceptions/unexpected-response.exception';
import { AbstractAcceptorStrategy } from '../abstract-acceptor-strategy';
import { EmptyResponseException } from '../../../exceptions/empty-response.exception';
import { Logger } from '@nestjs/common';
import { MessageTypeEnum } from '../../../enums/message-type.enum';
import { EventsEnum } from '../../../enums/events.enum';

export class ReplyAcceptorStrategy extends AbstractAcceptorStrategy {
  constructor(protected context: IAcceptionContext) {
    super(context);
  }

  accept(message: IMessage<any>): void {
    if (!this.context.requests[message.index]) {
      Logger.log(`Accept message with unexpected ${message.index}:`);
      return;
    }

    if (!message.payloadJson) {
      this.throwException(message.index, new EmptyResponseException());
      return;
    }

    const firstChar = message.payloadJson.charAt(0);

    if (firstChar !== '{' && firstChar !== '[') {
      this.throwException(message.index, new UnexpectedResponseException(message.payloadJson));
      return;
    }

    const response = message.getPayload(Result);

    if (response instanceof Result && response.result === false) {
      this.throwException(message.index, this.createException(response));
      return;
    }

    const payload = message.getPayload(this.context.requests[message.index].types.response);

    this.context.requests[message.index].resolve(payload);

    delete this.context.requests[message.index];
  }
}
