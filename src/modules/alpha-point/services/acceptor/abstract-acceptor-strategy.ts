import { IAcceptionContext, IAcceptorStrategy } from '../../interfaces/acceptor-strategy.interface';
import { IMessage } from '../../interfaces/message.interface';
import { Result } from '../../models/message';
import { BadGatewayException } from '@nestjs/common';
import { AlphaPointNotAuthorizedException } from '../../exceptions/alpha-point/alpha-point-not-authorized.exception';
import { AlphaPointInvalidResponseException } from '../../exceptions/alpha-point/alpha-point-invalid-response.exception';
import { AlphaPointOperationFailedException } from '../../exceptions/alpha-point/alpha-point-operation-failed.exception';
import { AlphaPointServerErrorException } from '../../exceptions/alpha-point/alpha-point-server-error.exception';
import { AlphaPointNotFoundException } from '../../exceptions/alpha-point/alpha-point-not-found.exception';
import { ErrorCodesEnum } from '../../enums/error-codes.enum';

export abstract class AbstractAcceptorStrategy implements IAcceptorStrategy {
  protected constructor(protected context: IAcceptionContext) {}

  abstract accept(message: IMessage<any>): void;

  protected createException(result: Result): BadGatewayException {
    switch (result.errorCode) {
      case ErrorCodesEnum.NotAuthorized:
        return new AlphaPointNotAuthorizedException(result);
      case ErrorCodesEnum.InvalidResponse:
        return new AlphaPointInvalidResponseException(result);
      case ErrorCodesEnum.OperationFailed:
        return new AlphaPointOperationFailedException(result);
      case ErrorCodesEnum.ServerError:
        return new AlphaPointServerErrorException(result);
      case ErrorCodesEnum.NotFound:
        return new AlphaPointNotFoundException(result);

      default:
        return new BadGatewayException(`${result.errorMessage}:\n${result.detail}`);
    }
  }

  protected throwException(requestIndex: number, exception: Error) {
    this.context.requests[requestIndex].reject(exception);
  }
}
