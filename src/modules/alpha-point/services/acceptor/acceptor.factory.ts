import { MessageTypeEnum } from '../../enums/message-type.enum';
import { IAcceptionContext, IAcceptorStrategy } from 'src/modules/alpha-point/interfaces/acceptor-strategy.interface';
import { InternalServerErrorException } from '@nestjs/common';
import { ReplyAcceptorStrategy } from '../../services/acceptor/strategies/reply.acceptor-strategy';
import { EventAcceptorStrategy } from '../../services/acceptor/strategies/event.acceptor-strategy';
import { ErrorAcceptorStrategy } from '../../services/acceptor/strategies/error.acceptor-strategy';

export class AcceptorFactory {
  static create(messageType: MessageTypeEnum, context: IAcceptionContext): IAcceptorStrategy {
    switch (messageType) {
      case MessageTypeEnum.Reply:
        return new ReplyAcceptorStrategy(context);
      case MessageTypeEnum.Event:
        return new EventAcceptorStrategy(context);
      case MessageTypeEnum.Error:
        return new ErrorAcceptorStrategy(context);

      default:
        throw new InternalServerErrorException(`Unexpected message type '${messageType}'`);
    }
  }
}
