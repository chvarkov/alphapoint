import { InternalServerErrorException } from '@nestjs/common';
import { TickerNormalizerStrategy } from '../../services/normalizer/strategies/ticker.normalizer-strategy';
import { TradeNormalizerStrategy } from '../../services/normalizer/strategies/trade.normalizer-strategy';
import { INormalizerStrategy } from '../../interfaces/normalizer-strategy.interface';
import { L2EventDataNormalizerStrategy } from './strategies/l2-event-data.normalizer-strategy';
import { Ticker, Trade, Level2EventData } from '@alpha-point/models/normalized';

export class NormalizerFactory {
  static create<T>(type: new () => T): INormalizerStrategy<T> {
    if (!type || !type.name) {
      throw new InternalServerErrorException('Not supported type.');
    }

    switch (type.name) {
      case Ticker.name:
        return new TickerNormalizerStrategy(type);
      case Trade.name:
        return new TradeNormalizerStrategy(type);
      case Level2EventData.name:
        return new L2EventDataNormalizerStrategy(type);

      default:
        throw new InternalServerErrorException('Not supported type.');
    }
  }
}
