import { INormalizerStrategy } from 'src/modules/alpha-point/interfaces/normalizer-strategy.interface';
import { InternalServerErrorException } from '@nestjs/common';
import { ClassType } from 'class-transformer/ClassTransformer';

export abstract class AbstractNormalizeStrategy<T, U> implements INormalizerStrategy<T> {
  protected constructor(protected type: new () => T) {}

  abstract normalize(array: any[]): T;

  abstract denormalize(model: T): any[];

  normalizeArray(twoDimensionalArray: any[][]): T[] {
    return twoDimensionalArray.map(value => this.normalize(value));
  }

  denormalizeArray(modelArray: T[]): any[][] {
    return modelArray.map(value => this.denormalize(value));
  }

  protected fetchAsProcessableType(cls: ClassType<U>, model: T): U {
    if (!(model instanceof cls)) {
      throw new InternalServerErrorException('Unprocessable model.');
    }

    return model;
  }

  protected createModel(data: U): T {
    return Object.assign(new this.type(), data);
  }
}
