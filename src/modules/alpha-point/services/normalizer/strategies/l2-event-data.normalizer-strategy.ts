import { AbstractNormalizeStrategy } from '../abstract-normalize-strategy';
import { Level2EventData } from '../../../models/normalized';

export class L2EventDataNormalizerStrategy<T> extends AbstractNormalizeStrategy<T, Level2EventData> {
  constructor(protected type: new () => T) {
    super(type);
  }

  normalize(array: any[]): T {
    return this.createModel({
      mdUpdateId: array[0],
      accountId: array[1],
      actionTimestamp: array[2],
      actionType: array[3],
      lastTradePrice: array[4],
      orderId: array[5],
      price: array[6],
      productPairCode: array[7],
      quantity: array[8],
      side: array[9],
    });
  }

  denormalize(value: T): any[] {
    const model = this.fetchAsProcessableType(Level2EventData, value);
    return [
      model.mdUpdateId,
      model.accountId,
      model.actionTimestamp,
      model.actionType,
      model.lastTradePrice,
      model.orderId,
      model.price,
      model.productPairCode,
      model.quantity,
      model.side,
    ];
  }
}
