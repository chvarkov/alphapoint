import { AbstractNormalizeStrategy } from '../../../services/normalizer/abstract-normalize-strategy';
import { Trade } from '../../../models/normalized/trade';

export class TradeNormalizerStrategy<T> extends AbstractNormalizeStrategy<T, Trade> {
  constructor(protected type: new () => T) {
    super(type);
  }

  normalize(array: any[]): T {
    return this.createModel({
      tradeId: array[0],
      productPairCode: array[1],
      quantity: array[2],
      price: array[3],
      order1: array[4],
      order2: array[5],
      tradeTime: array[6],
      direction: array[7],
      takerSide: array[8],
      blockTrade: !!array[9],
      orderClientId: array[10],
    });
  }

  denormalize(value: T): any[] {
    const model = this.fetchAsProcessableType(Trade, value);
    return [
      model.tradeId,
      model.productPairCode,
      model.quantity,
      model.price,
      model.order1,
      model.order2,
      model.tradeTime,
      model.direction,
      model.takerSide,
      +model.blockTrade,
      model.orderClientId,
    ];
  }
}
