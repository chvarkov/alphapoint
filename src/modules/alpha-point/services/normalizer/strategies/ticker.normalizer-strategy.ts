import { Ticker } from '../../../models/normalized/ticker';
import { AbstractNormalizeStrategy } from '../../../services/normalizer/abstract-normalize-strategy';

export class TickerNormalizerStrategy<T> extends AbstractNormalizeStrategy<T, Ticker> {
  constructor(protected type: new () => T) {
    super(type);
  }

  normalize(array: any[]): T {
    return this.createModel({
      time: array[0],
      high: array[1],
      low: array[2],
      open: array[3],
      close: array[4],
      volume: array[5],
      insideBidPrice: array[6],
      insideAskPrice: array[7],
      instrumentId: array[8],
    });
  }

  denormalize(value: T): any[] {
    const model = this.fetchAsProcessableType(Ticker, value);
    return [
      model.time,
      model.high,
      model.low,
      model.open,
      model.close,
      model.volume,
      model.insideBidPrice,
      model.insideAskPrice,
      model.instrumentId,
    ];
  }
}
