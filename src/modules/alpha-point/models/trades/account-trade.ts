import { OmsOption } from '../message';
import { Expose } from 'class-transformer';

export class AccountTradesRequest extends OmsOption {
  @Expose({ name: 'AccountId' })
  accountId: number;

  @Expose({ name: 'StartIndex' })
  startIndex: number;

  @Expose({ name: 'Count' })
  count: number;
}

export class AccountTrade {
  @Expose({ name: 'OMSId' })
  omsId: number;

  @Expose({ name: 'ExecutionId' })
  executionId: number;

  @Expose({ name: 'TradeId' })
  tradeId: number;

  @Expose({ name: 'OrderId' })
  orderId: number;

  @Expose({ name: 'AccountId' })
  accountId: number;

  @Expose({ name: 'AccountName' })
  accountName: string;

  @Expose({ name: 'SubAccountId' })
  subAccountId: number;

  @Expose({ name: 'ClientOrderId' })
  clientOrderId: number;

  @Expose({ name: 'InstrumentId' })
  instrumentId: number;

  @Expose({ name: 'Side' })
  side: string;

  @Expose({ name: 'OrderType' })
  orderType: string;

  @Expose({ name: 'Quantity' })
  quantity: number;

  @Expose({ name: 'RemainingQuantity' })
  remainingQuantity: number;

  @Expose({ name: 'Price' })
  price: number;

  @Expose({ name: 'Value' })
  value: number;

  @Expose({ name: 'CounterParty' })
  counterParty: string;

  @Expose({ name: 'OrderTradeRevision' })
  orderTradeRevision: number;

  @Expose({ name: 'Direction' })
  direction: number;

  @Expose({ name: 'IsBlockTrade' })
  isBlockTrade: boolean;

  @Expose({ name: 'Fee' })
  fee: number;

  @Expose({ name: 'FeeProductId' })
  feeProductId: number;

  @Expose({ name: 'OrderOriginator' })
  orderOriginator: number;

  @Expose({ name: 'UserName' })
  username: string;

  @Expose({ name: 'TradeTimeMS' })
  tradeTimeMS: number;

  @Expose({ name: 'MakerTaker' })
  makerTaker: string;

  @Expose({ name: 'AdapterTradeId' })
  adapterTradeId: number;

  @Expose({ name: 'InsideBid' })
  insideBid: number;

  @Expose({ name: 'InsideBidSize' })
  insideBidSize: number;

  @Expose({ name: 'InsideAsk' })
  insideAsk: number;

  @Expose({ name: 'InsideAskSize' })
  insideAskSize: number;

  @Expose({ name: 'IsQuote' })
  isQuote: boolean;

  @Expose({ name: 'TradeTime' })
  tradeTime: number;
}
