import { Expose } from 'class-transformer';

export class TickerHistoryRequest {
  @Expose({ name: 'InstrumentId' })
  instrumentId: number;

  @Expose({ name: 'FromDate' })
  fromDate: number;
}
