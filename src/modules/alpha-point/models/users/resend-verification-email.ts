import { Expose } from 'class-transformer';

export class ResendVerificationEmailRequest {
  @Expose({ name: 'Email' })
  email: string;
}
