import { Expose } from 'class-transformer';
import { OmsOption } from '../message';

export class LoggedInUserRequest extends OmsOption {
  @Expose({ name: 'SessionToken' })
  sessionToken: string;
}
