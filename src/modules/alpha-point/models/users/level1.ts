import { OmsOption } from '../message';
import { Expose } from 'class-transformer';

export class Level1 {
  exchangeId: number;
  productPairCode: number;
  bestBid: number;
  bestOffer: number;
  volume: number;
  lastTradedPx: number;
  lastTradedVolume: number;
  lastTradeTime: number;
  timeStamp: number;
  bidQty: number;
  askQty: number;
  bidOrderCt: number;
  askOrderCt: number;
  sessionOpen: number;
  sessionHigh: number;
  sessionLow: number;
  sessionClose: number;
  currentDayVolume: number;
  currentDayNumTrades: number;
  currentDayPxChange: number;
  rolling24HrVolume: number;
  rolling24NumTrades: number;
  rolling24HrPxChange: number;
  rolling24HrPxChangePercent: number;
}

export class Level1Subscribe extends OmsOption {
  @Expose({ name: 'InstrumentId' })
  instrumentId: number;
}

export class Level1BySymbolRequest extends OmsOption {
  @Expose({ name: 'Symbol' })
  symbol: number;
}

export class Level1EventData extends OmsOption {
  @Expose({ name: 'ExchangeId' })
  exchangeId: number;

  @Expose({ name: 'InstrumentId' })
  instrumentId: number;

  @Expose({ name: 'BestBid' })
  bestBid: number;

  @Expose({ name: 'BestOffer' })
  bestOffer: number;

  @Expose({ name: 'LastTradedPx' })
  lastTradedPx: number;

  @Expose({ name: 'LastTradedQty' })
  lastTradedQty: number;

  @Expose({ name: 'LastTradeTime' })
  lastTradeTime: number;

  @Expose({ name: 'SessionOpen' })
  sessionOpen: number;

  @Expose({ name: 'SessionHigh' })
  sessionHigh: number;

  @Expose({ name: 'SessionLow' })
  sessionLow: number;

  @Expose({ name: 'Volume' })
  volume: number;

  @Expose({ name: 'CurrentDayVolume' })
  currentDayVolume: number;

  @Expose({ name: 'CurrentDayNumTrades' })
  currentDayNumTrades: number;

  @Expose({ name: 'CurrentDayPxChange' })
  currentDayPxChange: number;

  @Expose({ name: 'Rolling24HrVolume' })
  rolling24HrVolume: number;

  @Expose({ name: 'Rolling24NumTrades' })
  rolling24NumTrades: number;

  @Expose({ name: 'Rolling24HrPxChange' })
  rolling24HrPxChange: number;

  @Expose({ name: 'TimeStamp' })
  timeStamp: number;
}
