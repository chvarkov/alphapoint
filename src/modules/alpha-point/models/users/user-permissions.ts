import { Expose } from 'class-transformer';

export class UserPermissionsRequest {
  @Expose({ name: 'UserId' })
  userId?: number;
}
