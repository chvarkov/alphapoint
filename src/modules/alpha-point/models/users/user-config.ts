import { Expose, Type } from 'class-transformer';
import { InternalServerErrorException } from '@nestjs/common';
import { IMap } from 'src/modules/alpha-point/interfaces/map.interface';

export class UserConfigRequest {
  @Expose({ name: 'UserId' })
  userId: number;

  @Expose({ name: 'UserName' })
  username: string;
}

export class UserConfigItem {
  @Expose({ name: 'Key' })
  key: string;

  @Expose({ name: 'Value' })
  value: string;
}

export class UserConfig {
  private configs: IMap<UserConfigItem> = {};

  private changes: string[] = [];

  constructor(configs: UserConfigItem[]) {
    configs.forEach(item => (this.configs[item.key] = item));
  }

  get(key: string): string | null {
    return this.has(key) ? this.configs[key].value : null;
  }

  getAll(): UserConfigItem[] {
    return Object.values(this.configs);
  }

  getChanges(): UserConfigItem[] {
    return this.getAll().filter(item => this.changes.includes(item.key));
  }

  has(key: string): boolean {
    return !!this.configs[key] && !!this.configs[key].value;
  }

  set(key: string, value: string): UserConfig {
    this.configs[key] = Object.assign(new UserConfigItem(), { key, value });
    this.changes.push(key);

    return this;
  }

  remove(key): UserConfig {
    if (!this.has(key)) {
      throw new InternalServerErrorException(`Config '${key}' does not exists.`);
    }

    this.configs[key].value = '';
    this.changes.push(key);

    return this;
  }
}

export class SetUserConfigRequest extends UserConfigRequest {
  @Type(() => UserConfigItem)
  @Expose({ name: 'Config' })
  config: UserConfigItem[];
}
