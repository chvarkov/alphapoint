import { Expose } from 'class-transformer';
import { PartialModel } from '../message';

export class WebAuthenticateUser extends PartialModel<WebAuthenticateUser> {
  @Expose({ name: 'Authenticated' })
  authenticated: boolean;

  @Expose({ name: 'errormsg' })
  errorMessage?: string;

  @Expose({ name: 'SessionToken' })
  sessionToken?: string;

  twoFaToken?: string;
}

export class Credentials extends PartialModel<Credentials> {
  @Expose({ name: 'UserName' })
  username: string;

  @Expose({ name: 'Password' })
  password: string;
}
