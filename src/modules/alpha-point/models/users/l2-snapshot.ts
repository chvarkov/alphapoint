import { OmsOption } from '../message';
import { Expose } from 'class-transformer';
import { L2SnapshotSideEnum } from '../../enums/l2-snapshot-side.enum';

export class L2SnapshotRequest extends OmsOption {
  @Expose({ name: 'InstrumentId' })
  instrumentId: number;

  @Expose({ name: 'Depth' })
  depth?: number = 100;
}

export class L2Snapshot {
  @Expose({ name: 'MDUpdateID' })
  mdUpdateId: number;

  @Expose({ name: 'Accounts' })
  accounts: number;

  @Expose({ name: 'ActionDateTime' })
  actionDateTime: number;

  @Expose({ name: 'ActionType' })
  actionType: number;

  @Expose({ name: 'LastTradePrice' })
  lastTradePrice: number;

  @Expose({ name: 'Orders' })
  orders: number;

  @Expose({ name: 'Price' })
  price: number;

  @Expose({ name: 'ProductPairCode' })
  productPairCode: number;

  @Expose({ name: 'Quantity' })
  quantity: number;

  @Expose({ name: 'Side' })
  side: L2SnapshotSideEnum;
}
