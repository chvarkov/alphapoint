import { Expose } from 'class-transformer';

export class UserInfoRequest {
  @Expose({ name: 'UserId' })
  userId?: number;

  @Expose({ name: 'UserName' })
  username?: string;

  @Expose({ name: 'Email' })
  email?: string;
}

export class UserInfo {
  @Expose({ name: 'UserId' })
  userId: number;

  @Expose({ name: 'UserName' })
  username: string;

  @Expose({ name: 'Email' })
  email: string;

  @Expose({ name: 'PasswordHash' })
  passwordHash: string;

  @Expose({ name: 'PendingEmailCode' })
  pendingEmailCode: string;

  @Expose({ name: 'EmailVerified' })
  emailVerified: boolean;

  @Expose({ name: 'AccountId' })
  accountId: number;

  @Expose({ name: 'DateTimeCreated' })
  dateTimeCreated: number;

  @Expose({ name: 'AffiliateId' })
  affiliateId: number;

  @Expose({ name: 'RefererId' })
  refererId: number;

  @Expose({ name: 'OMSId' })
  omsId: number;

  @Expose({ name: 'Use2FA' })
  use2FA: boolean;

  @Expose({ name: 'Salt' })
  salt: string;

  @Expose({ name: 'PendingCodeTime' })
  pendingCodeTime: string;

  @Expose({ name: 'Locked' })
  locked: boolean;

  @Expose({ name: 'LockedTime' })
  lockedTime: string;

  @Expose({ name: 'NumberOfFailedAttempt' })
  numberOfFailedAttempt: number;
}
