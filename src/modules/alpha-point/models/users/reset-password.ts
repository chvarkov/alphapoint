import { Expose } from 'class-transformer';

export class ResetPasswordRequest {
  @Expose({ name: 'UserName' })
  username: string;

  @Expose({ name: 'ReCaptcha' })
  reCaptcha: string;
}
