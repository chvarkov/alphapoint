import { PartialModel } from '../message';
import { Expose } from 'class-transformer';

export class Authenticate2FARequest {
  @Expose({ name: 'Code' })
  code: string;
}

export class Authenticate2FA {
  @Expose({ name: 'Authenticated' })
  authenticated: boolean;

  @Expose({ name: 'SessionToken' })
  sessionToken: string;
}
