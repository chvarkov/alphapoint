import { Expose } from 'class-transformer';
import { OmsOption } from '../message';

export class UserAccountsRequest extends OmsOption {
  userId: number;

  @Expose({ name: 'userName' })
  username: string;
}
