import { Expose } from 'class-transformer';
import { PartialModel } from '../message';

export class RegisterUserInfo extends PartialModel<RegisterUserInfo> {
  @Expose({ name: 'UserName' })
  username: string;

  @Expose({ name: 'Email' })
  email: string;

  @Expose({ name: 'PasswordHash' })
  passwordHash: string;
}

export class UserRegisterRequest {
  @Expose({ name: 'AffiliateTag' })
  affiliateTag: string;

  @Expose({ name: 'OperatorId' })
  operatorId: number;

  @Expose({ name: 'ReCaptcha' })
  reCaptcha: string;

  @Expose({ name: 'UserInfo' })
  info: RegisterUserInfo;
}
