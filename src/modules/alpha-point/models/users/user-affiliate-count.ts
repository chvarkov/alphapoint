import { Expose } from 'class-transformer';
import { OmsOption } from '../message';

export class UserAffiliateCountRequest extends OmsOption {
  @Expose({ name: 'UserId' })
  userId: number;
}

export class UserAffiliateCount {
  @Expose({ name: 'Count' })
  count: number;
}
