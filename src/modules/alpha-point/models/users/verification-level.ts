import { PartialModel } from '../message';
import { Expose } from 'class-transformer';

export class SetVerificationLevelRequest {
  @Expose({ name: 'UserId' })
  userId: number;

  @Expose({ name: 'VerificationLevel' })
  verificationLevel: number;
}
