import { OmsOption } from '../message';
import { AccountFeeProductTypeEnum, AccountRiskTypeEnum, AccountTypeEnum } from '../../enums/account-info.enums';
import { Expose } from 'class-transformer';

export class AccountInfoRequest extends OmsOption {
  accountId: number;
}

export class AccountInfo {
  @Expose({ name: 'OMSID' })
  omsId: number;

  @Expose({ name: 'AccountId' })
  accountId: number;

  @Expose({ name: 'AccountName' })
  accountName: string;

  @Expose({ name: 'AccountHandle' })
  accountHandle: string;

  @Expose({ name: 'FirmId' })
  firmId: string;

  @Expose({ name: 'FirmName' })
  firmName: string;

  @Expose({ name: 'AccountType' })
  accountType: AccountTypeEnum;

  @Expose({ name: 'FeeGroupID' })
  feeGroupId: number;

  @Expose({ name: 'ParentID' })
  parentId: number;

  @Expose({ name: 'RiskType' })
  riskType: AccountRiskTypeEnum;

  @Expose({ name: 'VerificationLevel' })
  verificationLevel: number;

  @Expose({ name: 'CreditTier' })
  creditTier: number;

  @Expose({ name: 'FeeProductType' })
  feeProductType: AccountFeeProductTypeEnum;

  @Expose({ name: 'FeeProduct' })
  feeProduct: number;

  @Expose({ name: 'RefererId' })
  refererId: number;

  @Expose({ name: 'LoyaltyProductId' })
  loyaltyProductId: number;

  @Expose({ name: 'LoyaltyEnabled' })
  loyaltyEnabled: boolean;

  @Expose({ name: 'MarginEnabled' })
  marginEnabled: boolean;

  @Expose({ name: 'LiabilityAccountId' })
  liabilityAccountId: number;

  @Expose({ name: 'LendingAccountId' })
  lendingAccountId: number;

  @Expose({ name: 'ProfitLossAccountId' })
  profitLossAccountId: number;
}
