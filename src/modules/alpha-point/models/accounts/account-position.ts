import { OmsOption } from '../message';
import { Expose } from 'class-transformer';

export class AccountPositionsRequest extends OmsOption {
  @Expose({ name: 'AccountId' })
  accountId: number;
}

export class AccountPositionItem {
  @Expose({ name: 'OMSId' })
  omsId: number;

  @Expose({ name: 'AccountId' })
  accountId: number;

  @Expose({ name: 'AccountName' })
  accountName: number;

  @Expose({ name: 'ProductSymbol' })
  productSymbol: string;

  @Expose({ name: 'ProductId' })
  productId: number;

  @Expose({ name: 'Amount' })
  amount: number;

  @Expose({ name: 'Hold' })
  hold: number;

  @Expose({ name: 'PendingDeposits' })
  pendingDeposits: number;

  @Expose({ name: 'PendingWithdraws' })
  pendingWithdraws: number;

  @Expose({ name: 'TotalDayDeposits' })
  totalDayDeposits: number;

  @Expose({ name: 'TotalMonthDeposits' })
  totalMonthDeposits: number;

  @Expose({ name: 'TotalYearDeposits' })
  totalYearDeposits: number;

  @Expose({ name: 'TotalYearDepositNotional' })
  totalYearDepositNotional: number;

  @Expose({ name: 'TotalDayWithdraws' })
  totalDayWithdraws: number;

  @Expose({ name: 'TotalMonthWithdraws' })
  totalMonthWithdraws: number;

  @Expose({ name: 'TotalYearWithdraws' })
  totalYearWithdraws: number;

  @Expose({ name: 'TotalYearWithdrawNotional' })
  totalYearWithdrawNotional: number;

  @Expose({ name: 'NotionalProductId' })
  notionalProductId: number;

  @Expose({ name: 'NotionalProductSymbol' })
  notionalProductSymbol: string;

  @Expose({ name: 'NotionalValue' })
  notionalValue: number;
}
