import { PartialModel } from '../message';

export class Level2EventData extends PartialModel<Level2EventData> {
  mdUpdateId: number;
  accountId: number;
  actionTimestamp: number;
  actionType: number;
  lastTradePrice: number;
  orderId: number;
  price: number;
  productPairCode: number; // Equivalent instrumentId
  quantity: number;
  side: number;
}
