import { TradeDirectionEnum } from '../../enums/trade-direction.enum';
import { TradeTakerSideEnum } from '../../enums/trade-taker-side.enum';
import { PartialModel } from '../message';

export class Trade extends PartialModel<Trade> {
  tradeId: number;
  productPairCode: number; // Equivalent instrumentId
  quantity: number;
  price: number;
  order1: number;
  order2: number;
  tradeTime: number;
  direction: TradeDirectionEnum;
  takerSide: TradeTakerSideEnum;
  blockTrade: boolean;
  orderClientId: number;
}
