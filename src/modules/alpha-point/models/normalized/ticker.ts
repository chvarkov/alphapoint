import { PartialModel } from '../message';

export class Ticker extends PartialModel<Ticker> {
  time: number;
  high: number;
  low: number;
  open: number;
  close: number;
  volume: number;
  insideBidPrice: number;
  insideAskPrice: number;
  instrumentId: number;
}
