import { OmsOption } from '../message';
import { Expose } from 'class-transformer';
import { TransactionReferenceTypeEnum, TransactionTypeEnum } from '../../enums/transaction.enums';

export class AccountTransactionsRequest extends OmsOption {
  @Expose({ name: 'AccountId' })
  accountId: number;

  @Expose({ name: 'Depth' })
  depth?: number;
}

export class Transaction {
  @Expose({ name: 'TransactionId' })
  transactionId: number;

  @Expose({ name: 'OMSId' })
  omsId: number;

  @Expose({ name: 'AccountId' })
  accountId: number;

  @Expose({ name: 'CR' })
  creditEntry: number;

  @Expose({ name: 'DR' })
  debitEntry: number;

  @Expose({ name: 'Counterparty' })
  counterparty: number;

  @Expose({ name: 'TransactionType' })
  transactionType: TransactionTypeEnum;

  @Expose({ name: 'ReferenceId' })
  referenceId: number;

  @Expose({ name: 'ReferenceType' })
  referenceType: TransactionReferenceTypeEnum;

  @Expose({ name: 'ProductId' })
  productId: number;

  @Expose({ name: 'Balance' })
  balance: number;

  @Expose({ name: 'TimeStamp' })
  timestamp: number;
}
