import { OmsOption } from '../message';
import { Expose, Transform } from 'class-transformer';
import { DepositStatusEnum } from '../../enums/deposit-status.enum';
import { arrayDeserialize } from '../../helpers/deserializer.helper';

export class DepositTicketsRequest extends OmsOption {
  @Expose({ name: 'OperatorId' })
  operatorId: number;

  @Expose({ name: 'StartIndex' })
  startIndex?: number;

  @Expose({ name: 'Limit' })
  limit?: number;
}

export class DepositInfo {
  @Expose({ name: 'Full Name' })
  fullName: string;

  @Expose({ name: 'Comments' })
  comment: string;

  language: string;
}

export class DepositComment {
  // TODO: Check structure
}

export class DepositAttachment {
  // TODO: Check structure
}

export class DepositTicket {
  @Expose({ name: 'AssetManagerId' })
  assetManagerId: number;

  @Expose({ name: 'AccountId' })
  accountId: number;

  @Expose({ name: 'AssetId' })
  assetId: number;

  @Expose({ name: 'AccountName' })
  accountName?: string;

  @Expose({ name: 'AssetName' })
  assetName: number;

  @Expose({ name: 'Amount' })
  amount: number;

  @Expose({ name: 'NotionalValue' })
  notionalValue: number;

  @Expose({ name: 'NotionalProductId' })
  notionalProductId: number;

  @Expose({ name: 'OMSId' })
  omsId: number;

  @Expose({ name: 'RequestCode' })
  requestCode: string;

  @Expose({ name: 'ReferenceId' })
  referenceId: string;

  @Expose({ name: 'RequestIP' })
  requestIP: string;

  @Expose({ name: 'RequestUser' })
  requestUser: number;

  @Expose({ name: 'RequestUserName' })
  requestUserName: string;

  @Expose({ name: 'OperatorId' })
  operatorId: number;

  @Expose({ name: 'Status' })
  status: DepositStatusEnum;

  @Expose({ name: 'FeeAmt' })
  feeAmt: number;

  @Expose({ name: 'UpdatedByUser' })
  updatedByUser: number;

  @Expose({ name: 'UpdatedByUserName' })
  updatedByUserName: string;

  @Expose({ name: 'TicketNumber' })
  ticketNumber: number;

  @Expose({ name: 'DepositInfo' })
  depositInfo: DepositInfo;

  @Expose({ name: 'RejectReason' })
  rejectReason?: string;

  @Expose({ name: 'CreatedTimestamp' })
  createdTimestamp: string;

  @Expose({ name: 'LastUpdateTimeStamp' })
  lastUpdateTimeStamp: string;

  @Expose({ name: 'CreatedTimestampTick' })
  createdTimestampTick: number;

  @Expose({ name: 'LastUpdateTimeStampTick' })
  lastUpdateTimeStampTick: number;

  @Expose({ name: 'Comments' })
  @Transform((values: string[]) => arrayDeserialize(DepositAttachment, values))
  comments: DepositComment[];

  @Expose({ name: 'Attachments' })
  @Transform((values: string[]) => arrayDeserialize(DepositAttachment, values))
  attachments?: null;
}
