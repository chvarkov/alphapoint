import { OmsOption } from '../message';
import { deserialize, Expose, Transform } from 'class-transformer';
import { WithdrawStatusEnum } from '../../enums/withdraw-status.enum';
import { arrayDeserialize } from '../../helpers/deserializer.helper';
import { DepositAttachment } from '../../models/tickets';

export class AllWithdrawTicketsRequest extends OmsOption {
  @Expose({ name: 'OperatorId' })
  operatorId: number;
}

export class WithdrawTicketTemplateForm {
  @Expose({ name: 'TemplateType' })
  templateType: string;

  @Expose({ name: 'Comment' })
  comment: string;

  @Expose({ name: 'ExternalAddress' })
  externalAddress: string;
}

export class WithdrawTicketComment {
  // TODO: Check structure
  commentId: number;
  enteredBy: number;
  enteredDateTime: string;
  comment: string;
  operatorId: number;
  omsId: number;
  ticketCode: string;
  ticketId: number;
}

export class WithdrawTicketAttachment {
  // TODO: Check structure
  attachmentId: number;
  submittedByUserId: number;
  submittedByUserName: string;
  uploadDate: string;
  uploadIP: string;
  ticketNumber: number;
}

export class WithdrawTransactionDetails {
  @Expose({ name: 'TxId' })
  txId: string;

  @Expose({ name: 'ExternalAddress' })
  externalAddress: string;

  @Expose({ name: 'Amount' })
  amount: number;

  @Expose({ name: 'Confirmed' })
  confirmed: boolean;

  @Expose({ name: 'LastUpdated' })
  lastUpdated: string;

  @Expose({ name: 'TimeSubmitted' })
  timeSubmitted: string;

  @Expose({ name: 'AccountProviderName' })
  accountProviderName: string;
}

export class WithdrawTicket {
  @Expose({ name: 'AssetManagerId' })
  assetManagerId: number;

  @Expose({ name: 'AccountId' })
  accountId: number;

  @Expose({ name: 'AccountName' })
  accountName?: string;

  @Expose({ name: 'AssetId' })
  assetId: number;

  @Expose({ name: 'AssetName' })
  assetName: string;

  @Expose({ name: 'Amount' })
  amount: number;

  @Expose({ name: 'NotionalValue' })
  notionalValue: number;

  @Expose({ name: 'NotionalProductId' })
  notionalProductId: number;

  @Expose({ name: 'TemplateForm' })
  @Transform(value => deserialize(WithdrawTicketTemplateForm, value))
  templateForm: WithdrawTicketTemplateForm;

  @Expose({ name: 'TemplateFormType' })
  templateFormType: string;

  @Expose({ name: 'OMSId' })
  omsId: number;

  @Expose({ name: 'RequestCode' })
  requestCode: string;

  @Expose({ name: 'EmailRequestCode' })
  emailRequestCode?: string;

  @Expose({ name: 'RequestIP' })
  requestIP: string;

  @Expose({ name: 'RequestUserId' })
  requestUserId: number;

  @Expose({ name: 'RequestUserName' })
  requestUserName: string;

  @Expose({ name: 'OperatorId' })
  operatorId: number;

  @Expose({ name: 'Status' })
  status: WithdrawStatusEnum;

  @Expose({ name: 'FeeAmt' })
  feeAmt: number;

  @Expose({ name: 'UpdatedByUser' })
  updatedByUser: number;

  @Expose({ name: 'UpdatedByUserName' })
  updatedByUserName: string;

  @Expose({ name: 'TicketNumber' })
  ticketNumber: number;

  @Expose({ name: 'WithdrawTransactionDetails' })
  @Transform(value => (value ? deserialize(WithdrawTransactionDetails, value) : null))
  withdrawTransactionDetails?: WithdrawTransactionDetails;

  @Expose({ name: 'RejectReason' })
  rejectReason?: string;

  @Expose({ name: 'CreatedTimestamp' })
  createdTimestamp: string;

  @Expose({ name: 'LastUpdateTimestamp' })
  lastUpdateTimestamp: string;

  @Expose({ name: 'CreatedTimestampTick' })
  createdTimestampTick: number;

  @Expose({ name: 'LastUpdateTimestampTick' })
  lastUpdateTimestampTick: number;

  @Expose({ name: 'Comments' })
  @Transform((values: string[]) => arrayDeserialize(DepositAttachment, values))
  comments: WithdrawTicketComment[];

  @Expose({ name: 'Attachments' })
  @Transform((values: string[]) => arrayDeserialize(DepositAttachment, values))
  attachments: WithdrawTicketAttachment[];

  @Expose({ name: 'AuditLog' })
  @Transform(values => arrayDeserialize(DepositAttachment, values))
  auditLog: any[];
}
