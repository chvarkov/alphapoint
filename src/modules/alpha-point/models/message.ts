import { MessageTypeEnum } from '../enums/message-type.enum';
import { PrivateMethodsEnum } from 'src/modules/alpha-point/enums/common/private-methods.enum';
import { PublicMethodsEnum } from 'src/modules/alpha-point/enums/common/public-methods.enum';
import { Expose, plainToClass } from 'class-transformer';
import { ClassType } from 'class-transformer/ClassTransformer';
import { IMessage } from 'src/modules/alpha-point/interfaces/message.interface';
import { ErrorCodesEnum } from '../enums/error-codes.enum';
import { EndpointType } from '../services/connection';

export class PartialModel<T> {
  constructor(partial: Partial<T> = {}) {
    Object.assign(this, partial);
  }
}

export class Message<T> extends PartialModel<Message<T>> implements IMessage<T> {
  @Expose({ name: 'm' })
  type: MessageTypeEnum;

  @Expose({ name: 'i' })
  index: number;

  @Expose({ name: 'n' })
  method: EndpointType;

  @Expose({ name: 'o' })
  payloadJson: string;

  getPayload(payloadClass: ClassType<T> = null): T | T[] {
    const payload = JSON.parse(this.payloadJson);

    if (!Array.isArray(payload)) {
      return plainToClass(payloadClass, payload);
    }

    return payload.map(value => plainToClass(payloadClass, value));
  }
}

export class Result {
  result: boolean;

  @Expose({ name: 'errormsg' })
  errorMessage?: string;

  @Expose({ name: 'errorcode' })
  errorCode?: ErrorCodesEnum;

  detail?: string;
}

export class OmsOption extends PartialModel<OmsOption> {
  @Expose({ name: 'OMSId' })
  omsId?: number;
}

export class InstrumentAwareRequest extends OmsOption {
  @Expose({ name: 'InstrumentId' })
  instrumentId: number;
}
