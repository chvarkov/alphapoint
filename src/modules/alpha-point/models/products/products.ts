import { Expose, Transform, TransformClassToClass, Type } from 'class-transformer';
import { ProductTypeOptionEnum } from '../../enums/product-type-option.enum';
import { OmsOption } from '../message';

export class ProductRequest extends OmsOption {
  @Expose({ name: 'ProductId' })
  productId: number;
}

export class Product extends OmsOption {
  @Expose({ name: 'ProductId' })
  productId: number;

  @Expose({ name: 'Product' })
  product: string;

  @Expose({ name: 'ProductFullName' })
  fullName: string;

  @Expose({ name: 'ProductType' })
  type: ProductTypeOptionEnum;

  @Expose({ name: 'DecimalPlaces' })
  decimalPlaces: number;

  @Expose({ name: 'TickSize' })
  tickSize: number;

  @Expose({ name: 'NoFees' })
  noFees: boolean;
}
