import { OmsOption } from './message';
import { Expose } from 'class-transformer';

export class SubscribeTradesRequest extends OmsOption {
  @Expose({ name: 'InstrumentId' })
  instrumentId: number;

  @Expose({ name: 'IncludeLastCount' })
  includeLastCount?: number = 100;
}
