import { OmsOption } from './message';
import { Expose } from 'class-transformer';

export class SubscribeTickerRequest extends OmsOption {
  @Expose({ name: 'InstrumentId' })
  instrumentId: number;

  @Expose({ name: 'Interval' })
  interval?: number = 60;

  @Expose({ name: 'IncludeLastCount' })
  includeLastCount?: number = 100;
}
