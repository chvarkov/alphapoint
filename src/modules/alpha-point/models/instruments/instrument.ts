import { OmsOption } from '../message';
import { Expose } from 'class-transformer';
import { InstrumentSessionStatusEnum, InstrumentTypeEnum } from '../../enums/instrument.enums';

export class InstrumentRequest extends OmsOption {
  @Expose({ name: 'InstrumentId' })
  instrumentId: number;
}

export class Instrument extends OmsOption {
  @Expose({ name: 'InstrumentId' })
  instrumentId: number;

  @Expose({ name: 'Symbol' })
  symbol: string;

  @Expose({ name: 'Product1' })
  product1: number;

  @Expose({ name: 'Product1Symbol' })
  product1Symbol: string;

  @Expose({ name: 'Product2' })
  product2: number;

  @Expose({ name: 'Product2Symbol' })
  product2Symbol: string;

  @Expose({ name: 'InstrumentType' })
  type: InstrumentTypeEnum;

  @Expose({ name: 'VenueInstrumentId' })
  venueInstrumentId: number;

  @Expose({ name: 'VenueId' })
  venueId: number;

  @Expose({ name: 'SortIndex' })
  sortIndex: number;

  @Expose({ name: 'SessionStatus' })
  sessionStatus: InstrumentSessionStatusEnum;

  @Expose({ name: 'PreviousSessionStatus' })
  previousSessionStatus: InstrumentSessionStatusEnum;

  @Expose({ name: 'SessionStatusDateTime' })
  sessionStatusDateTime: string;

  @Expose({ name: 'SelfTradePrevention' })
  selfTradePrevention: boolean;

  @Expose({ name: 'QuantityIncrement' })
  quantityIncrement: number;
}
