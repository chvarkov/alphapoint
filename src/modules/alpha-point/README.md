### AlphaPoint module

[AlphaPoint API documentation](https://alphapoint.github.io/slate/)

#### Configuration
```typescript
import { AlphaPointModule } from 'src/modules/alpha-point/alpha-point.module';

@Module({
  imports: [
    AlphaPointModule.forRoot({
      public: {
        connection: {
          wss: 'wss://your-alpha-point-url/WSGateway',
          options: {},
        },
        attemptCount: 3,
        reconnectTimeout: 2000,
      },
      private: {
        connection: {
          wss: 'wss://your-alpha-point-url/WSAdminGateway',
          options: {},
        },
        credentials: {
          userName: 'username',
          password: 'pa$$w0rd',
        },
        attemptCount: 3,
        reconnectTimeout: 2000,
      },
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

```

#### Normalization / Denormalization

Some endpoints provide information in an array. 
For example endpoint `SubscribeTicker` return data in next format:
```typescript
  const response = [
      1501603632000, // DateTime - UTC - Milliseconds since 1/1/1970
      2700.33,       // High 
      2701.01,       // Low
      2687.01,       // Open
      2687.01,       // Close
      24.86100992,   // Volume
      0,             // Inside Bid Price
      2870.95,       // Inside Ask Price
      1,             // InstrumentId
  ];
```

You can use the following functions to convert data of this format.
```typescript
// Convert array to model
const ticker: Ticker = normalize(Ticker, response);

// Convert model to array
const array: any[] = denormalize(ticker);

// Convert two-dimensional array to array of models
const tickers: Ticker[] = normalizeArray(Ticker, [array]);

// Convert array of models to two-dimensional array
const twoDimensionalArray = denormalizeArray(tickers);
```


List of endpoints that return information as an array.

Endpoint | Response type | Model for normalization | Connection
:--- |  :---: | :---: | :---:
`GetTickerHistory` | number[][] | [Ticker](models/normalized/ticker.ts) | Public
`SubscribeTicker` | number[][] | [Ticker](models/normalized/ticker.ts) | Public
`SubscribeTrades` | number[][] | [Trade](models/normalized/trade.ts) | Public

#### Overview

###### Public methods:

Endpoint | Request model | Response Model 
:--- | :--- | :---
`GetInstrument` | [InstrumentRequest](models/instruments/instrument.ts) | [Instrument](models/instruments/instrument.ts) 
`GetInstruments` | void | [Instrument](models/instruments/instrument.ts)[]
`GetProduct` | [ProductRequest](models/products/products.ts) | [Product](models/products/products.ts)
`GetProducts` | void | [Product](models/products/products.ts)[] 
`GetLevel1` | [Level1Request](models/users/level1.ts) | [Level1](models/users/level1.ts) 
`GetL2Snapshot` | [L2SnapshotRequest](models/users/l2-snapshot.ts) | [L2Snapshot](models/users/l2-snapshot.ts) 
`GetTickerHistory` | [TickerHistoryRequest](models/ticker-history.ts) | number[][] 

###### Public subscriptions:

Subscribe | Unsubscribe | Request model | Response/Event Model
:--- | :--- | :--- | :---
`SubscribeLevel1` | `UnsubscribeLevel1` | [InstrumentAwareRequest](models/message.ts) | [Level1EventData](models/users/level1.ts)
`SubscribeLevel2` | `UnsubscribeLevel2` | [InstrumentAwareRequest](models/message.ts) | number[][]
`SubscribeTicker` | `UnsubscribeTicker` | [InstrumentAwareRequest](models/message.ts) | number[][]
`SubscribeTrades` | `UnsubscribeTrades` | [InstrumentAwareRequest](models/message.ts) | number[][]

###### Private methods:

Endpoint | Request model | Response Model 
:--- | :--- | :---
`GetAvailablePermissionList` | void | string[]
`Authenticate2FA` | [Authenticate2FARequest](models/users/authenticate-2fa.ts) | [Authenticate2FA](models/users/authenticate-2fa.ts) 
`RegisterUser` | [UserRegisterRequest](models/users/user-register.ts) | void 
`SetUserVerificationLevel` | [SetVerificationLevelRequest](models/users/verification-level.ts) | void 
`GetUserAccounts` | [UserAccountsRequest](models/users/user-accounts.ts) | number[] 
`GetUserAffiliateCount` | [UserAffiliateCountRequest](models/users/user-affiliate-count.ts) | [UserAffiliateCount](models/users/user-affiliate-count.ts)
`GetUserConfig` | [UserConfigRequest](models/users/user-config.ts) | [UserConfig](models/users/user-config.ts)
`SetUserConfig` | [SetUserConfigRequest](models/users/user-config.ts) | void 
`GetUserInfo` | [UserInfoRequest](models/users/user-info.ts) | [UserInfo](models/users/user-info.ts) 
`GetUserPermissions` | [UserPermissionsRequest](models/users/user-permissions.ts) | string[]  
`GetAllUnredactedUserConfigsForUser` | [UserConfigRequest](models/users/user-config.ts) | [UserConfig](models/users/user-config.ts)
`ForceUserLogoff` | [ForceUserLogoffRequest](models/users/force-user-logoff.ts) | void 
`ResendVerificationEmail` | [ResendVerificationEmailRequest](models/users/resend-verification-email.ts) | void 
`GetLoggedInUserBySessionToken` | [LoggedInUserRequest](models/users/logged-in-user.ts) | TODO 
`GetAccountInfo` | [AccountInfoRequest](models/accounts/account-info.ts) | [AccountInfo](models/accounts/account-info.ts) 
`GetAccountPositions` | [AccountPositionsRequest](models/accounts/account-position.ts) | [AccountPositionItem](models/accounts/account-position.ts)[]
`GetAccountTrades` | [AccountTradesRequest](models/trades/account-trade.ts) | [AccountTrade](models/trades/account-trade.ts)[] 
`GetAllWithdrawTickets` | [AllWithdrawTicketsRequest](models/tickets/withdraw-ticket.ts) | [WithdrawTicket](models/tickets/withdraw-ticket.ts) 
`GetAccountWithdrawTransactions` | [AccountTransactionsRequest](models/tickets/transaction.ts) | [Transaction](models/tickets/transaction.ts)[]
