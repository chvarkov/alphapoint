import { Type } from '@nestjs/common';
import { deserialize } from 'class-transformer';

export function arrayDeserialize<T>(type: Type<T>, array?: string[]): T[] {
  return array ? array.map(value => deserialize(type, value)) : [];
}
