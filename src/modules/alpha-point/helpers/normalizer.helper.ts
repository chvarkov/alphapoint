import { NormalizerFactory } from '../services/normalizer/normalizer.factory';

export function normalize<T>(type: new () => T, array: any[]): T {
  return NormalizerFactory.create<T>(type).normalize(array);
}

export function denormalize<T>(object: T): any[] {
  return NormalizerFactory.create(object.constructor as new () => T).denormalize(object);
}

export function normalizeArray<T>(type: new () => T, array: any[][]): T[] {
  return NormalizerFactory.create(type).normalizeArray(array);
}

export function denormalizeArray<T>(objects: T[]): any[][] {
  return objects.length > 0
    ? NormalizerFactory.create(objects[0].constructor as new () => T).denormalizeArray(objects)
    : [];
}
