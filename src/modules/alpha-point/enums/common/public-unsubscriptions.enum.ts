export enum PublicUnsubscriptionsEnum {
  UnsubscribeLevel1 = 'UnsubscribeLevel1',
  UnsubscribeLevel2 = 'UnsubscribeLevel2',
  UnsubscribeTicker = 'UnsubscribeTicker',
  UnsubscribeTrades = 'UnsubscribeTrades',
}
