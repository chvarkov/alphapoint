export enum PrivateMethodsEnum {
  // Admin

  GetAvailablePermissionList = 'GetAvailablePermissionList',

  // Users

  WebAuthenticateUser = 'WebAuthenticateUser',
  Authenticate2FA = 'Authenticate2FA',
  RegisterUser = 'RegisterUser',
  SetUserVerificationLevel = 'SetUserVerificationLevel',
  GetUserAccounts = 'GetUserAccounts',
  GetUserAffiliateCount = 'GetUserAffiliateCount',
  GetUserConfig = 'GetUserConfig',
  SetUserConfig = 'SetUserConfig',
  GetUserInfo = 'GetUserInfo',
  GetUserPermissions = 'GetUserPermissions',
  GetAllUnredactedUserConfigsForUser = 'GetAllUnredactedUserConfigsForUser',
  ForceUserLogoff = 'ForceUserLogoff',
  ResendVerificationEmail = 'ResendVerificationEmail',
  GetLoggedInUserBySessionToken = 'GetLoggedInUserBySessionToken',

  // Accounts

  GetAccountInfo = 'GetAccountInfo',
  GetAccountPositions = 'GetAccountPositions',

  // Trades

  GetAccountTrades = 'GetAccountTrades',

  // Tickets

  GetAllWithdrawTickets = 'GetAllWithdrawTickets',
  GetAccountWithdrawTransactions = 'GetAccountWithdrawTransactions',
  GetAllDepositTickets = 'GetAllDepositTickets',
  GetAccountDepositTransactions = 'GetAccountDepositTransactions',
}
