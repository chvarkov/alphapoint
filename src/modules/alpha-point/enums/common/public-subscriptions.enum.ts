export enum PublicSubscriptionsEnum {
  SubscribeLevel1 = 'SubscribeLevel1',
  SubscribeLevel2 = 'SubscribeLevel2',
  SubscribeTicker = 'SubscribeTicker',
  SubscribeTrades = 'SubscribeTrades',
}
