export enum MessageTypeEnum {
  Request,
  Reply,
  Subscribe,
  Event,
  Unsubscribe,
  Error,
}
