export enum AccountTypeEnum {
  Asset = 'Asset',
  Liability = 'Liability',
  ProfitLoss = 'ProfitLoss',
}

export enum AccountRiskTypeEnum {
  Unknown = 'Unknown',
  Normal = 'Normal',
  NoRiskCheck = 'NoRiskCheck',
  NoTrading = 'NoTrading',
}

export enum AccountFeeProductTypeEnum {
  BaseProduct = 'BaseProduct',
  SingleProduct = 'SingleProduct',
}
