export enum EventsEnum {
  TickerDataUpdateEvent = 'TickerDataUpdateEvent',
  Level1UpdateEvent = 'Level1UpdateEvent',
  Level2UpdateEvent = 'Level2UpdateEvent',
  OrderTradeEvent = 'OrderTradeEvent',
  LogoutEvent = 'LogoutEvent',
}
