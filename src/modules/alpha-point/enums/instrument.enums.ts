export enum InstrumentSessionStatusEnum {
  Unknown = 'Unknown',
  Running = 'Running',
  Paused = 'Paused',
  Stopped = 'Stopped',
  Starting = 'Starting',
}

export enum InstrumentTypeEnum {
  Unknown = 'Unknown',
  Standard = 'Standard',
}
