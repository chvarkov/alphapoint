export enum TransactionTypeEnum {
  Fee = 1,
  Trade = 2,
  Other = 3,
  Reverse = 4,
  Hold = 5,
  Rebate = 6,
  MarginAcquisition = 7,
  MarginRelinquish = 8,
}

export enum TransactionReferenceTypeEnum {
  Trade = 1,
  Deposit = 2,
  Withdraw = 3,
  Transfer = 4,
  OrderHold = 5,
  WithdrawHold = 6,
  DepositHold = 7,
  MarginHold = 8,
  ManualHold = 9,
  ManualEntry = 10,
  MarginAcquisition = 11,
  MarginRelinquish = 12,
  MarginQuoteHold = 13,
}
