export enum L2SnapshotSideEnum {
  Buy,
  Sell,
  Short,
  Unknown,
}

export enum L2EventDataActionTypeEnum {
  New,
  Update,
  Deletion,
}
