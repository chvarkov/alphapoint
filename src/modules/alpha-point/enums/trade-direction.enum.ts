export enum TradeDirectionEnum {
  NoChange,
  UpTick,
  DownTick,
}
