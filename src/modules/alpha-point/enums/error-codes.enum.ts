export enum ErrorCodesEnum {
  NotAuthorized = 20,
  InvalidResponse = 100,
  OperationFailed = 101,
  ServerError = 102,
  NotFound = 104,
}
