export enum ProductTypeOptionEnum {
  Unknown = 'Unknown',
  NationalCurrency = 'NationalCurrency',
  CryptoCurrency = 'CryptoCurrency',
  Contract = 'Contract',
}
