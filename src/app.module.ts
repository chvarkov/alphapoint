import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AlphaPointModule } from '@alpha-point/alpha-point.module';
import { CoreModule } from '@core/core.module';
import { ConfigService } from '@core/services/config.service';

const config = new ConfigService();

@Module({
  imports: [
    AlphaPointModule.forRoot({
      private: config.alphaPointPrivateConnectionOptions,
      public: config.alphaPointPublicConnectionOptions,
    }),
    CoreModule,
  ],
  controllers: [AppController],
  providers: [AppService, ConfigService],
})
export class AppModule {}
