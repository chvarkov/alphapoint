import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from '@core/filters/http-exception-filter';
import * as cookieParser from 'cookie-parser';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(AppModule, new FastifyAdapter());

  const exceptionFilter = app.get<HttpExceptionFilter>(HttpExceptionFilter);

  app
    .useGlobalFilters(exceptionFilter)
    .use(cookieParser());

  await app.listen(3000);
}
bootstrap();
