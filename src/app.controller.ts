import { Controller, Get, InternalServerErrorException, Logger, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { PublicService } from './modules/alpha-point/services/public.service';
import { PrivateService } from './modules/alpha-point/services/private.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly apPublic: PublicService,
    private readonly apPrivate: PrivateService,
  ) {}

  @Get()
  async getHello(): Promise<any> {
    throw new InternalServerErrorException('ISE');
    // try {
    //   return await this.apPublic.getTickerHistory({
    //     instrumentId: 3,
    //     fromDate: 1568581012785,
    //   });
    // } catch (e) {
    //   throw new InternalServerErrorException(e.message.message);
    // }
  }
}
