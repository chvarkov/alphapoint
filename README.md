## NDAX Backend

### Installation

```bash
$ npm install
```

### Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

### Test

```bash
# all tests
$ npm run test

# test functional
$ npm run test:functional

# test integration
$ npm run test:integration

# test coverage
$ npm run test:cov

```
